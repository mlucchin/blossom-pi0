import pandas as pd
import numpy as np
import networkx as nx
import itertools
import time
import os, sys
import ROOT
import math
from hep_functions import *

def blossom_matching(FILENAME,M_TRUE,SIGMAS=3,TOLERANCE=0.001,E_RES=0.3, ACUT = 1, BCUT = 1, maxEvents=10000000, WCUT = True):
    '''
    INPUT:
        FILENAME = directory and filename of the event to match
        M_TRUE = invariant mass of the process to be matched
        DEFAULT_INPUTS:
            SIGMAS = 3             # SIGMAS*TOLERANCE = minimum cutoff
            TOLERANCE = 0.001      # SIGMAS*TOLERANCE = minimum cutoff
            E_RES = 0.3            # SIGMAS*(TOLERANCE+E_RES) = cutoff value
            FILENAME = ''          # string directory and filename
            
    OUTPUT:
        RETURNS matches (list of lists): each element in matches is a list of tuples; each tuple contains the node_id of the matched gamma
    '''
    
    # Matches are recorded in a list of list of 2-tuple
    matches = []
    ### Load Data
    ev0 = pd.read_csv(f'{FILENAME}',names=['event_id','mother_id','px','py','pz','energy'],sep=' ',index_col=False).sort_values('event_id')
    # group by event number
    group0 = ev0.groupby('event_id')
    TOT_EVENTS = len(group0)
    print("Number of events: " + str(TOT_EVENTS))
    print("SCUT: " + str(E_RES) + ", ACUT: " + str(ACUT) + ", BCUT: " + str(BCUT) + ", maxEvents: " + str(maxEvents) )
    EV_NUM = max(ev0.event_id)
    if (maxEvents<TOT_EVENTS): TOT_EVENTS = maxEvents

    # count matches 
    true_matches_count = []
    false_matches_count = []
    dupli_matches_count = []
    event_count = []
    pi0ingammgamma_events_count = []
    
    frac_trueGammaPi0 = []
    frac_true = []
    frac_fake = []
    frac_tot  = []
    
    clusteredPi0               = [] 
    unClusteredPhotons         = []
    clusteredPi0_withDaughters = [] 
    
    start = time.time()
    #######################
    for i, event in group0:
        if (i<maxEvents):
            print(f"Processing events... event number {i} out of {EV_NUM}",end='\r')
            # create list of all possible pairs            
            # event.index --> array with all the IDs (unique index) of my photons
            # itertools.combinations(event.index,2) --> tutte le combinazioni di 2 elementi tranne un elemento con se stesso
            # list(zip(event.index,event.index)) --> zip creates a tuple from elements of two arrays
            # columns , associate a name (dictionary) to a column, 
            
            edges = pd.DataFrame(data = list(itertools.combinations(event.index,2)), columns = ['node1','node2'])
            
            energy = event['energy'].to_dict()
            momentum = event[['px','py','pz']].to_dict()
            mothers = event['mother_id']
            n_event = event['event_id']
            #print (n_event)

            # initialize node attributes to edges dataframe (using photon index)
            edges = initialize (edges)
            
            #assign real physical entries to the attributes (columns) of the dataframe for each photon index
            edges = set_edge_values (edges, energy, momentum, mothers)

            ## Computing weights
            edges = edges.sort_values(['node1','node2'])
                        
            edges['ANGLE']      = get_angle (edges)                                    
            edges['BOOST']      = get_boost (edges)
            edges['M']          = get_inv_mass (edges)                                                            
            #edges['WEIGHT']     = get_weight (edges)
            edges['WEIGHT']     = get_pdf_weight (edges, str(E_RES))
                                    
            
            edges['e_tup'] = [tuple(sorted(i)) for i in list(zip(edges.node1.values,edges.node2.values))]
    
    
            this_fracPhotonsFromPi0 = float(len(edges[(edges.mother1==edges.mother2) ] )*2 ) / float(len(event))            
            frac_trueGammaPi0.append(this_fracPhotonsFromPi0)
            totNPhotons = float(len(event)) 
            
            
            # chi-squared cut-off (invariant mass squared difference cut-off)
            cut_off = SIGMAS*(E_RES)+TOLERANCE
            
            if (WCUT): edges = edges[(np.abs(edges['M']-M_TRUE)/M_TRUE<=cut_off) & (edges['ANGLE']<=ACUT) & (edges['BOOST']>=BCUT)]
            
            
            this_fracPhotonsFromPi0 = float(len(edges[(edges.mother1==edges.mother2)])*2 ) / float(len(event))
            
            
            #######################
            ### Build graph
            G = nx.from_pandas_edgelist(edges[['node1','node2','WEIGHT','mother1','mother2']],'node1','node2',edge_attr=['WEIGHT','mother1','mother2'])
            ### compute best matches following blossom algorithm 
            # (https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.matching.max_weight_matching.html)
                
            matched = [tuple(sorted(i)) for i in list(nx.algorithms.matching.max_weight_matching(G,weight='WEIGHT'))]
            #######################
    
            matched_photons = list(edges[(edges.e_tup.isin(matched))].e_tup)

            spare_photons = [photon for photon in event.index if photon not in list(sum(matched_photons, ()))]


            a = from_matched_to_attributes(matched_photons,event)
            
            APPR = 11
            #build collection of clustered pi0 w/ w/o daughters and of spare photons 
            for i_pair in range(len(a)):                
                p1_x = a[i_pair][list(a[i_pair].keys())[0]]['px']                
                p1_y = a[i_pair][list(a[i_pair].keys())[0]]['py']                
                p1_z = a[i_pair][list(a[i_pair].keys())[0]]['pz']                
                e1   = a[i_pair][list(a[i_pair].keys())[0]]['energy']                            
                p2_x = a[i_pair][list(a[i_pair].keys())[1]]['px']                
                p2_y = a[i_pair][list(a[i_pair].keys())[1]]['py']                
                p2_z = a[i_pair][list(a[i_pair].keys())[1]]['pz']                
                e2   = a[i_pair][list(a[i_pair].keys())[1]]['energy']            
                this_pair = [a[i_pair][list(a[i_pair].keys())[1]]['event_id'], round(p1_x,APPR), round(p1_y,APPR), round(p1_z,APPR), round(e1,APPR), round(p2_x,APPR), round(p2_y,APPR), round(p2_z,APPR), round(e2,APPR)]
                clusteredPi0_withDaughters.append(this_pair)
                
            b = from_unmatched_to_attributes(spare_photons,event)    
            for i_g in b.keys():
                p1_x = b[i_g]['px']                
                p1_y = b[i_g]['py']  
                p1_z = b[i_g]['pz']  
                e1 = b[i_g]['energy']                  
                this_gamma = [b[i_g]['event_id']  , round(p1_x,APPR), round(p1_y,APPR), round(p1_z,APPR), round(e1,APPR)]
                unClusteredPhotons.append(this_gamma)
            
            
            
            # count matches for event x
            #
            this_ntrue_pairs = len(edges[((edges.e_tup.isin(matched)))&(edges.mother1==edges.mother2)])            
            true_matches_count.append(this_ntrue_pairs)
            
            this_nfake_pairs = len(edges[((edges.e_tup.isin(matched)))&(edges.mother1!=edges.mother2)])               
            false_matches_count.append(this_nfake_pairs)            
            
            event_count.append(len(event.drop_duplicates('mother_id')))                        
            pi0ingammgamma_events_count.append(len(edges[(edges.mother1==edges.mother2)]))
            
            
            frac_true.append( float(this_ntrue_pairs*2./totNPhotons))
            frac_fake.append( float(this_nfake_pairs*2./totNPhotons))
            frac_tot.append ( float((this_ntrue_pairs+this_nfake_pairs)*2./totNPhotons))
            
            matches.append(list(edges[(edges.e_tup.isin(matched))].e_tup))
            
    end = time.time()
    print('Processing complete!')
    print('AGE: ',end - start, ', NEVENTS to process: ', TOT_EVENTS, ' :: TIME / EVENT = ', (end-start)/TOT_EVENTS)
    
    
    
    N_TRUE_FRAC  = float(np.sum(np.array(frac_true)) /len(frac_true))
    N_FALSE_FRAC = float(np.sum(np.array(frac_fake)) /len(frac_true))
    N_ALL_FRAC   = float(np.sum(np.array(frac_tot))  /len(frac_true))
    N_EXP_FRAC   = float(np.sum(np.array(frac_trueGammaPi0))  /len(frac_trueGammaPi0))
    
    
    print(f'Matches found: {N_TRUE_FRAC+N_FALSE_FRAC} [{N_TRUE_FRAC} True-positive and {N_FALSE_FRAC} False-positive] out of {N_EXP_FRAC} in {TOT_EVENTS} events')
    
    summary = [N_ALL_FRAC, N_TRUE_FRAC, N_FALSE_FRAC, N_EXP_FRAC, clusteredPi0_withDaughters, unClusteredPhotons, float(end - start)/TOT_EVENTS]
        
    
    return summary
    
    
    
    
    
    
    
def return_pdf(FILENAME,M_TRUE,SIGMAS=3,TOLERANCE=0.001,E_RES=0.3, ACUT = 1, BCUT = 1, maxEvents=10000000, WCUT = True):
    '''
    INPUT:
        FILENAME = directory and filename of the event to match
        M_TRUE = invariant mass of the process to be matched
        DEFAULT_INPUTS:
            SIGMAS = 3             # SIGMAS*TOLERANCE = minimum cutoff
            TOLERANCE = 0.001      # SIGMAS*TOLERANCE = minimum cutoff
            E_RES = 0.3            # SIGMAS*(TOLERANCE+E_RES) = cutoff value
            FILENAME = ''          # string directory and filename
            
    OUTPUT:
        RETURNS matches (list of lists): each element in matches is a list of tuples; each tuple contains the node_id of the matched gamma
    '''
    
    ### Load Data
    ev0 = pd.read_csv(f'{FILENAME}',names=['event_id','mother_id','px','py','pz','energy'],sep=' ',index_col=False).sort_values('event_id')
    # group by event number
    group0 = ev0.groupby('event_id')
    TOT_EVENTS = len(group0)
    print("Number of events: " + str(TOT_EVENTS))
    print("SCUT: " + str(E_RES) + ", ACUT: " + str(ACUT) + ", BCUT: " + str(BCUT) + ", maxEvents: " + str(maxEvents) )
    EV_NUM = max(ev0.event_id)
    if (maxEvents<TOT_EVENTS): TOT_EVENTS = maxEvents

    #pdfs lists
    all_angle_list = []
    true_angle_list = []
    fake_angle_list = []
    
    all_boost_list = []
    true_boost_list = []
    fake_boost_list = []
    
    all_mass_list = []
    true_mass_list = []
    fake_mass_list = []
    
    all_weight_list = []
    true_weight_list = []
    fake_weight_list = []
    
        
    start = time.time()
    #######################
    for i, event in group0:
        if (i<maxEvents):
            
            print(f"Processing events... event number {i} out of {EV_NUM}",end='\r')
            
            # create list of all possible pairs                        
            edges = pd.DataFrame(data = list(itertools.combinations(event.index,2)), columns = ['node1','node2'])
            
            energy = event['energy'].to_dict()
            momentum = event[['px','py','pz']].to_dict()
            mothers = event['mother_id']
            n_event = event['event_id']
            #print (n_event)

            # initialize node attributes to edges dataframe (using photon index)
            edges = initialize (edges)
            
            #assign real physical entries to the attributes (columns) of the dataframe for each photon index
            edges = set_edge_values (edges, energy, momentum, mothers)
            

            ## Computing weights for all edges
            edges = edges.sort_values(['node1','node2'])
                
            
            edges['ANGLE']      = get_angle (edges)                                    
            edges['BOOST']      = get_boost (edges)
            edges['M']          = get_inv_mass (edges)                                                            
            edges['WEIGHT']     = get_weight (edges)
            
            cut_off = SIGMAS*(E_RES)+TOLERANCE                        
            if (WCUT): edges = edges[(np.abs(edges['M']-M_TRUE)/M_TRUE<=cut_off)]
            
            all_angle_list.append(edges['ANGLE'])
            all_boost_list.append(edges['BOOST'])
            all_mass_list.append(edges['M'])
            all_weight_list.append(edges['WEIGHT'])
            
            
            #control plots with true edges
            true_edges = edges.copy()            
            true_edges = true_edges[(true_edges.mother1==true_edges.mother2)]
            
            true_angle_list.append(true_edges['ANGLE'])
            true_boost_list.append(true_edges['BOOST'])
            true_mass_list.append(true_edges['M'])
            true_weight_list.append(true_edges['WEIGHT'])
                                                                                                            
            
            #control plots with fake edges
            fake_edges = edges.copy()            
            fake_edges = fake_edges[(fake_edges.mother1!=fake_edges.mother2)]
            
            fake_angle_list.append(fake_edges['ANGLE'])
            fake_boost_list.append(fake_edges['BOOST'])
            fake_mass_list.append(fake_edges['M'])
            fake_weight_list.append(fake_edges['WEIGHT'])
            
            
                        
            
    end = time.time()
    print('Processing complete!')
    print('AGE: ',end - start, ', NEVENTS to process: ', TOT_EVENTS, ' :: TIME / EVENT = ', (end-start)/TOT_EVENTS)
    
    pdf_list = [all_angle_list,  true_angle_list,  fake_angle_list, 
                all_boost_list,  true_boost_list,  fake_boost_list, 
                all_mass_list,   true_mass_list,   fake_mass_list,
                all_weight_list, true_weight_list, fake_weight_list]
            
    return pdf_list
    
    
    
    
    
    
    
    
    
def return_graph(FILENAME, selEvent, M_TRUE,SIGMAS=3,TOLERANCE=0.001,E_RES=0.3, ACUT = 1, BCUT = 1,maxEvents=10000000, WCUT = True):
    
    ev0 = pd.read_csv(f'{FILENAME}',names=['event_id','mother_id','px','py','pz','energy'],sep=' ',index_col=False).sort_values('event_id')
    # group by event number
    group0 = ev0.groupby('event_id')
    TOT_EVENTS = len(group0)
    print("Number of events: " + str(TOT_EVENTS))
    print("SCUT: " + str(E_RES) + ", ACUT: " + str(ACUT) + ", BCUT: " + str(BCUT) + ", maxEvents: " + str(maxEvents) )
    EV_NUM = max(ev0.event_id)
    if (maxEvents<TOT_EVENTS): TOT_EVENTS = maxEvents

    event = group0[selEvent]
        
    edges = pd.DataFrame(data = list(itertools.combinations(event.index,2)), columns = ['node1','node2'])
            
    energy = event['energy'].to_dict()
    momentum = event[['px','py','pz']].to_dict()
    mothers = event['mother_id']
    n_event = event['event_id']
    
    # assigning node attributes to edges dataframe
    edges = initialize (edges)
            
    #assign real physical entries to the attributes (columns) of the dataframe for each photon index
    edges = set_edge_values (edges, energy, momentum, mothers)
            
    ## Computing weights for all edges
    edges = edges.sort_values(['node1','node2'])
                
    edges['ANGLE']      = get_angle (edges)                                    
    edges['BOOST']      = get_boost (edges)
    edges['M']          = get_inv_mass (edges)                                                            
    edges['WEIGHT']     = get_weight (edges)

    cut_off = SIGMAS*(E_RES)+TOLERANCE            
    if (WCUT): edges = edges[(np.abs(edges['M']-M_TRUE)/M_TRUE<=cut_off) & (edges['ANGLE']<=ACUT) & (edges['BOOST']>=BCUT)]
    
    #######################
    ### Build graph
    G = nx.from_pandas_edgelist(edges[['node1','node2','weight','mother1','mother2']],'node1','node2',edge_attr=['weight','mother1','mother2'])
    
    return G;
    
    
    
    
    
    
    
    
    
def from_matched_to_attributes(MATCHED,EVENT):
    """ 
        The idea is super-simple:
        EVENT.loc[photon1].to_dict() take the EVENT DataFrame 
        and select the attibutes of the "photon"
        RETURNS: a list of dicts (containing 2 photon names and each name a dict of attributes)
    """
    attributes = []
    for photon1,photon2 in MATCHED:
        attributes.append(EVENT.loc[[photon1,photon2]].T.to_dict())
    return attributes

def from_unmatched_to_attributes(UNMATCHED,EVENT):
    return EVENT.loc[UNMATCHED].T.to_dict()


def truncate(number, decimals=0):
    """
    Returns a value truncated to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer.")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more.")
    elif decimals == 0:
        return math.trunc(number)

    factor = 10.0 ** decimals
    return math.trunc(number * factor) / factor
  
    
