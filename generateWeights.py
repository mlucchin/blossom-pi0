import os, sys
import ROOT

from ROOT import TCanvas, TGraphErrors, TLegend, TH1F, gPad, gStyle, TFile, TH2F
from ROOT import kGreen, kBlue, kRed, kYellow, kBlack, NULL
from blossom import *


gStyle.SetTitleXOffset (1.00) ;                                                                                        
gStyle.SetTitleYOffset (1.2) ;                                                                                                                                                                                                                 
gStyle.SetPadLeftMargin (0.13) ;                                                                                       
gStyle.SetPadBottomMargin (0.13) ;                                                                                                                                                                                                              
gStyle.SetTitleSize (0.05, "xyz") ;                                                                                    
gStyle.SetLabelSize (0.035,"xyz") ;  
    
gStyle.SetLegendBorderSize(0);
gStyle.SetLegendFillColor(0);
gStyle.SetLegendFont(42);
gStyle.SetLegendTextSize(0.035);
#TLegend leg;
  

#res_string   = ["0.0", "0.01", "0.03", "0.05", "0.1", "0.15", "0.3"]
#res          = [0, 0.01, 0.03, 0.05, 0.10, 0.15,  0.3]

res_string   = ["0.03"]
res          = [0.03]

sample = ["hzz_bbqqqq"]

sigma_cut = 3

WCUT = [True]

files = []


M_TRUE = 0.134976      # invariant mass of the matched events
TOLERANCE = 0.0001      # SIGMAS*TOLERANCE = minimum cutoff        
maxEvents = 100

#histos
hAngle_All = []
hAngle_True = []
hAngle_Fake = []

hBoost_All = []
hBoost_True = []
hBoost_Fake = []

hMass_All = []
hMass_True = []
hMass_Fake = []

hWeight_All = []
hWeight_True = []
hWeight_Fake = []

#graphs
gAngle_All = []
gAngle_True = []
gAngle_Fake = []

gBoost_All = []
gBoost_True = []
gBoost_Fake = []

gMass_All = []
gMass_True = []
gMass_Fake = []





NBINS = 100

for iRes in range(len(res)):
    
    hAngle_All.append (TH1F("hAngle_All_%s"%res_string[iRes], "hAngle_All_%s"%res_string[iRes], 100, 0, 1))
    hAngle_True.append (TH1F("hAngle_True_%s"%res_string[iRes], "hAngle_True_%s"%res_string[iRes], 100, 0, 1))
    hAngle_Fake.append (TH1F("hAngle_Fake_%s"%res_string[iRes], "hAngle_Fake_%s"%res_string[iRes], 100, 0, 1))
    
    hBoost_All.append (TH1F("hBoost_All_%s"%res_string[iRes], "hBoost_All_%s"%res_string[iRes], 500, 0, 100))
    hBoost_True.append (TH1F("hBoost_True_%s"%res_string[iRes], "hBoost_True_%s"%res_string[iRes], 500, 0, 100))
    hBoost_Fake.append (TH1F("hBoost_Fake_%s"%res_string[iRes], "hBoost_Fake_%s"%res_string[iRes], 500, 0, 100))
    
    hMass_All.append (TH1F("hMass_All_%s"%res_string[iRes], "hMass_All_%s"%res_string[iRes], NBINS, 0, 1))
    hMass_True.append (TH1F("hMass_True_%s"%res_string[iRes], "hMass_True_%s"%res_string[iRes], NBINS, 0, 1))
    hMass_Fake.append (TH1F("hMass_Fake_%s"%res_string[iRes], "hMass_Fake_%s"%res_string[iRes], NBINS, 0, 1))
    
    hWeight_All.append (TH1F("hWeight_All_%s"%res_string[iRes], "hWeight_All_%s"%res_string[iRes], 2000, 0.99, 1.01))
    hWeight_True.append (TH1F("hWeight_True_%s"%res_string[iRes], "hWeight_True_%s"%res_string[iRes], 2000, 0.99, 1.01))
    hWeight_Fake.append (TH1F("hWeight_Fake_%s"%res_string[iRes], "hWeight_Fake_%s"%res_string[iRes], 2000, 0.99, 1.01))
    
    gAngle_All.append(TGraphErrors())
    gAngle_True.append(TGraphErrors())
    gAngle_Fake.append(TGraphErrors())

    gBoost_All.append(TGraphErrors())
    gBoost_True.append(TGraphErrors())
    gBoost_Fake.append(TGraphErrors())

    gMass_All.append(TGraphErrors())
    gMass_True.append(TGraphErrors())
    gMass_Fake.append(TGraphErrors())
    
    
    
                  

for iSample in range(len(sample)):
    print("sample loop[%s]"%str(iSample))
    
    for iMode in range(len(WCUT)):
        for iRes in range(len(res)):
            print("res loop[%s]"%str(iRes))
            FILENAME = str('./input/%s__dumped_photons_res%s.txt'% (sample[iSample], res_string[iRes]))    
            
            my_pdf = return_pdf(FILENAME, M_TRUE, sigma_cut, TOLERANCE, res[iRes], 1, 1, maxEvents, WCUT[iMode])                                            
            
            for i_list in range (12):                            
                for my_list in my_pdf[i_list]:
                    for i in my_list:
                        if   (i_list == 0)  : hAngle_All[iRes].Fill(i)
                        elif (i_list == 1)  : hAngle_True[iRes].Fill(i)
                        elif (i_list == 2)  : hAngle_Fake[iRes].Fill(i)
                        elif (i_list == 3)  : hBoost_All[iRes].Fill(i)
                        elif (i_list == 4)  : hBoost_True[iRes].Fill(i)
                        elif (i_list == 5)  : hBoost_Fake[iRes].Fill(i)
                        elif (i_list == 6)  : hMass_All[iRes].Fill(i)
                        elif (i_list == 7)  : hMass_True[iRes].Fill(i)
                        elif (i_list == 8)  : hMass_Fake[iRes].Fill(i)
                        elif (i_list == 9)  : hWeight_All[iRes].Fill(i)
                        elif (i_list == 10) : hWeight_True[iRes].Fill(i)
                        elif (i_list == 11) : hWeight_Fake[iRes].Fill(i)
                
                

                


cMassPDF = []

for iRes in range (len(res)):
    
    cMassPDF.append(TCanvas("cMassPDF_"+str(res[iRes]), "cMassPDF_"+str(res[iRes]), 500, 500))
    
    cMassPDF[iRes].cd()
    norm_const = hMass_All[iRes].Integral()
    if (norm_const <= 0) : continue
    

    hMass_All[iRes].Scale(1./norm_const)
    print ("integral after normalization = " + str(hMass_All[iRes].Integral()))
    hMass_All[iRes].SetStats(0)
    hMass_All[iRes].SetTitle("Invariant mass of photon pairs");
    hMass_All[iRes].GetXaxis().SetTitle("M_{#gamma_{1}, #gamma_{2}} [GeV]")
    hMass_All[iRes].GetYaxis().SetTitle("Probability")
    hMass_All[iRes].GetXaxis().SetRangeUser(0,1)
    hMass_All[iRes].GetYaxis().SetRangeUser(0,hMass_All[iRes].GetMaximum()*1.3)
    hMass_All[iRes].Draw()
    hMass_All[iRes].SetLineColor(kBlack)
    hMass_All[iRes].SetMarkerColor(kBlack)

    #hMass_True[iRes].Scale(1./norm_const)    
    hMass_True[iRes].Scale(1./hMass_True[iRes].Integral())    
    hMass_True[iRes].Draw("same")
    hMass_True[iRes].SetLineColor(kGreen+1)
    hMass_True[iRes].SetMarkerColor(kGreen+1)
    
    #hMass_Fake[iRes].Scale(1./norm_const)    
    hMass_Fake[iRes].Scale(1./hMass_Fake[iRes].Integral())    
    hMass_Fake[iRes].Draw("same")
    hMass_Fake[iRes].SetLineColor(kRed+1)
    hMass_Fake[iRes].SetMarkerColor(kRed+1)
    
    
    leg = TLegend(0.5,0.72,0.88,0.88,"","brNDC");
    leg.AddEntry(hMass_All[iRes], "All photon pairs", "lp");         
    leg.AddEntry(hMass_True[iRes], "True #pi^{0}", "lp");         
    leg.AddEntry(hMass_Fake[iRes], "Fake photon pairs", "lp");         
    leg.Draw();

    
    for iBin in range(hMass_All[iRes].GetNbinsX()):        
        gMass_All[iRes].SetPoint(iBin,  hMass_All[iRes].GetBinCenter(iBin+1),  hMass_All[iRes].GetBinContent(iBin+1))
        gMass_True[iRes].SetPoint(iBin, hMass_True[iRes].GetBinCenter(iBin+1), hMass_True[iRes].GetBinContent(iBin+1))
        gMass_Fake[iRes].SetPoint(iBin, hMass_Fake[iRes].GetBinCenter(iBin+1), hMass_Fake[iRes].GetBinContent(iBin+1))
    
    cMassPDF[iRes].Update()


cBoostPDF = []

for iRes in range (len(res)):
    
    cBoostPDF.append(TCanvas("cBoostPDF_"+str(res[iRes]), "cBoostPDF_"+str(res[iRes]), 500, 500))
    
    cBoostPDF[iRes].cd()
    norm_const = hBoost_All[iRes].Integral()
    if (norm_const <= 0) : continue
    

    hBoost_All[iRes].Scale(1./norm_const)
    print ("integral after normalization = " + str(hBoost_All[iRes].Integral()))
    hBoost_All[iRes].SetStats(0)
    hBoost_All[iRes].SetTitle("Boost of photon pairs");
    hBoost_All[iRes].GetXaxis().SetTitle("E_{#pi^{0}} / M_{{#pi^{0}}")
    hBoost_All[iRes].GetYaxis().SetTitle("Probability")
    hBoost_All[iRes].GetXaxis().SetRangeUser(0,10)
    hBoost_All[iRes].GetYaxis().SetRangeUser(0,hBoost_All[iRes].GetMaximum()*1.3)
    hBoost_All[iRes].Draw()
    hBoost_All[iRes].SetLineColor(kBlack)
    hBoost_All[iRes].SetMarkerColor(kBlack)

    #hBoost_True[iRes].Scale(1./norm_const)    
    hBoost_True[iRes].Scale(1./hBoost_True[iRes].Integral())    
    hBoost_True[iRes].Draw("same")
    hBoost_True[iRes].SetLineColor(kGreen+1)
    hBoost_True[iRes].SetMarkerColor(kGreen+1)
    
    #hBoost_Fake[iRes].Scale(1./norm_const)    
    hBoost_Fake[iRes].Scale(1./hBoost_Fake[iRes].Integral())    
    hBoost_Fake[iRes].Draw("same")
    hBoost_Fake[iRes].SetLineColor(kRed+1)
    hBoost_Fake[iRes].SetMarkerColor(kRed+1)
    
    
    leg = TLegend(0.5,0.72,0.88,0.88,"","brNDC");
    leg.AddEntry(hBoost_All[iRes], "All photon pairs", "lp");         
    leg.AddEntry(hBoost_True[iRes], "True #pi^{0}", "lp");         
    leg.AddEntry(hBoost_Fake[iRes], "Fake photon pairs", "lp");         
    leg.Draw();
    #gPad.SetLogy()
    
    for iBin in range(hBoost_All[iRes].GetNbinsX()):        
        gBoost_All[iRes].SetPoint(iBin,  hBoost_All[iRes].GetBinCenter(iBin+1),  hBoost_All[iRes].GetBinContent(iBin+1))
        gBoost_True[iRes].SetPoint(iBin, hBoost_True[iRes].GetBinCenter(iBin+1), hBoost_True[iRes].GetBinContent(iBin+1))
        gBoost_Fake[iRes].SetPoint(iBin, hBoost_Fake[iRes].GetBinCenter(iBin+1), hBoost_Fake[iRes].GetBinContent(iBin+1))
    
    cBoostPDF[iRes].Update()


cAnglePDF = []

for iRes in range (len(res)):
    
    cAnglePDF.append(TCanvas("cAnglePDF_"+str(res[iRes]), "cAnglePDF_"+str(res[iRes]), 500, 500))
    
    cAnglePDF[iRes].cd()
    norm_const = hAngle_All[iRes].Integral()
    if (norm_const <= 0) : continue
    

    hAngle_All[iRes].Scale(1./norm_const)
    print ("integral after normalization = " + str(hAngle_All[iRes].Integral()))
    hAngle_All[iRes].SetStats(0)
    hAngle_All[iRes].SetTitle("Angle of photon pairs");
    hAngle_All[iRes].GetXaxis().SetTitle("E_{#pi^{0}} / M_{{#pi^{0}}")
    hAngle_All[iRes].GetYaxis().SetTitle("Probability")
    hAngle_All[iRes].GetXaxis().SetRangeUser(0,1)
    hAngle_All[iRes].GetYaxis().SetRangeUser(0,hAngle_All[iRes].GetMaximum()*1.3)
    hAngle_All[iRes].Draw()
    hAngle_All[iRes].SetLineColor(kBlack)
    hAngle_All[iRes].SetMarkerColor(kBlack)

    #hAngle_True[iRes].Scale(1./norm_const)    
    hAngle_True[iRes].Scale(1./hAngle_True[iRes].Integral())    
    
    hAngle_True[iRes].Draw("same")
    hAngle_True[iRes].SetLineColor(kGreen+1)
    hAngle_True[iRes].SetMarkerColor(kGreen+1)
    
    #hAngle_Fake[iRes].Scale(1./norm_const)    
    hAngle_Fake[iRes].Scale(1./hAngle_Fake[iRes].Integral())    
    hAngle_Fake[iRes].Draw("same")
    hAngle_Fake[iRes].SetLineColor(kRed+1)
    hAngle_Fake[iRes].SetMarkerColor(kRed+1)
    
    
    leg = TLegend(0.5,0.72,0.88,0.88,"","brNDC");
    leg.AddEntry(hAngle_All[iRes], "All photon pairs", "lp");         
    leg.AddEntry(hAngle_True[iRes], "True #pi^{0}", "lp");         
    leg.AddEntry(hAngle_Fake[iRes], "Fake photon pairs", "lp");         
    leg.Draw();
    #gPad.SetLogy()
    
    for iBin in range(hAngle_All[iRes].GetNbinsX()):        
        gAngle_All[iRes].SetPoint(iBin,  hAngle_All[iRes].GetBinCenter(iBin+1),  hAngle_All[iRes].GetBinContent(iBin+1))
        gAngle_True[iRes].SetPoint(iBin, hAngle_True[iRes].GetBinCenter(iBin+1), hAngle_True[iRes].GetBinContent(iBin+1))
        gAngle_Fake[iRes].SetPoint(iBin, hAngle_Fake[iRes].GetBinCenter(iBin+1), hAngle_Fake[iRes].GetBinContent(iBin+1))
    
    cAnglePDF[iRes].Update()
    
    
cWeightPDF = []

for iRes in range (len(res)):
    
    cWeightPDF.append(TCanvas("cWeightPDF_"+str(res[iRes]), "cWeightPDF_"+str(res[iRes]), 500, 500))
    
    cWeightPDF[iRes].cd()
    norm_const = hWeight_All[iRes].Integral()
    if (norm_const <= 0) : continue
    

    hWeight_All[iRes].Scale(1./norm_const)
    print ("integral after normalization = " + str(hWeight_All[iRes].Integral()))
    hWeight_All[iRes].SetStats(0)
    hWeight_All[iRes].SetTitle("Weight of photon pairs");
    hWeight_All[iRes].GetXaxis().SetTitle("E_{#pi^{0}} / M_{{#pi^{0}}")
    hWeight_All[iRes].GetYaxis().SetTitle("Probability")
    #hWeight_All[iRes].GetXaxis().SetRangeUser(0,10)
    hWeight_All[iRes].GetYaxis().SetRangeUser(0,hWeight_All[iRes].GetMaximum()*1.3)
    hWeight_All[iRes].Draw()
    hWeight_All[iRes].SetLineColor(kBlack)
    hWeight_All[iRes].SetMarkerColor(kBlack)

    hWeight_True[iRes].Scale(1./norm_const)    
    hWeight_True[iRes].Draw("same")
    hWeight_True[iRes].SetLineColor(kGreen+1)
    hWeight_True[iRes].SetMarkerColor(kGreen+1)
    
    hWeight_Fake[iRes].Scale(1./norm_const)    
    hWeight_Fake[iRes].Draw("same")
    hWeight_Fake[iRes].SetLineColor(kRed+1)
    hWeight_Fake[iRes].SetMarkerColor(kRed+1)
    
    
    leg = TLegend(0.5,0.72,0.88,0.88,"","brNDC");
    leg.AddEntry(hWeight_All[iRes], "All photon pairs", "lp");         
    leg.AddEntry(hWeight_True[iRes], "True #pi^{0}", "lp");         
    leg.AddEntry(hWeight_Fake[iRes], "Fake photon pairs", "lp");         
    leg.Draw();
    
    cWeightPDF[iRes].Update()
    
    
#dumping pdf's to root_file
outputFile = TFile("my_pdf.root", "RECREATE")
outputFile.cd()

for iRes in range (len(res)):
    gMass_All[iRes].SetName("gMass_All_"+str(res[iRes]))
    gMass_All[iRes].Write()
    gMass_True[iRes].SetName("gMass_True_"+str(res[iRes]))
    gMass_True[iRes].Write()
    gMass_Fake[iRes].SetName("gMass_Fake_"+str(res[iRes]))
    gMass_Fake[iRes].Write()
    
    gBoost_All[iRes].SetName("gBoost_All_"+str(res[iRes]))
    gBoost_All[iRes].Write()
    gBoost_True[iRes].SetName("gBoost_True_"+str(res[iRes]))
    gBoost_True[iRes].Write()
    gBoost_Fake[iRes].SetName("gBoost_Fake_"+str(res[iRes]))
    gBoost_Fake[iRes].Write()
    
    gAngle_All[iRes].SetName("gAngle_All_"+str(res[iRes]))
    gAngle_All[iRes].Write()
    gAngle_True[iRes].SetName("gAngle_True_"+str(res[iRes]))
    gAngle_True[iRes].Write()
    gAngle_Fake[iRes].SetName("gAngle_Fake_"+str(res[iRes]))
    gAngle_Fake[iRes].Write()
    
outputFile.Write()
outputFile.Close()





input('Press ENTER to exit')



