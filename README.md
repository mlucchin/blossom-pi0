Algorithm for photon pairing into mother pi0 mesons within multi-jet events from ZH events at e+e- colliders.


# INSTRUCTIONS
to generate a list of weights for photon pairs based on the pdf of invariant diphoton mass, boost and angular aperture of true and fake photon pairs
 
$ python3 generateWeights.py

The list of weights is saved in "my_pdf.root"
To run the photon to pi0 clustering algorithm
 
$ python3 optimizeBlossom.py

# COMMENTS
 
A set of useful functions are stored in hep_functions.py

The clustering algorithm is defined in blossom.py
This file also contains the hard cuts perfomed on the edges before running the Blossom V algorithm and the definition of weights to be used.
More input files can be downloaded at: https://cernbox.cern.ch/index.php/s/W5kpblPEldRnxaX





# old macros
Run as:
$ python3 runBlossom.py MAXEVENTS SAMPLE SIGMACUT ACUT BCUT EM_RES

Example:
$ python3 runBlossom.py 200 hzz_bbqqqq 3 1 1 0.03

The scripts uses input files from the input folder (list of photons of multi-jet events).
The script return output files which are lists of clustered photons and unclustered photons.


