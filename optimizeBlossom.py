import os, sys
import ROOT

from ROOT import TCanvas, TGraphErrors, TLegend, TH1F, gPad, gStyle, TFile, TH2F
from ROOT import kGreen, kBlue, kRed, kYellow, kBlack, NULL
from blossom import blossom_matching


gStyle.SetTitleXOffset (1.00) ;                                                                                        
gStyle.SetTitleYOffset (1.2) ;                                                                                                                                                                                                                 
gStyle.SetPadLeftMargin (0.13) ;                                                                                       
gStyle.SetPadBottomMargin (0.13) ;                                                                                                                                                                                                              
gStyle.SetTitleSize (0.05, "xyz") ;                                                                                    
gStyle.SetLabelSize (0.035,"xyz") ;  
    
gStyle.SetLegendBorderSize(0);
gStyle.SetLegendFillColor(0);
gStyle.SetLegendFont(42);
gStyle.SetLegendTextSize(0.035);
#TLegend leg;
  

#res_string   = ["0.0", "0.01", "0.03", "0.05", "0.1", "0.15", "0.3"]
#res          = [0, 0.01, 0.03, 0.05, 0.10, 0.15,  0.3]

res_string   = ["0.03"]
res          = [0.03]

sample = ["hzz_bbqqqq"]
#sample = [ "hzz_bbqqqq", "hbb_znunu", "hbb_zqq",]

sigma_cut = 3
acut = 0.98
bcut = 1

WCUT = [True]

files = []


M_TRUE = 0.134976      # invariant mass of the matched events
TOLERANCE = 0.0001      # SIGMAS*TOLERANCE = minimum cutoff        
maxEvents = 100


gFracClustered_vsRes = []
gFracTrueMatch_vsRes = []
gFracFakeMatch_vsRes = []
gFracTrueExp_vsRes = []
gAge_vsRes = []


for iSample in range(len(sample)):
    
    temp_list_1 = []
    temp_list_2 = []
    temp_list_3 = []
    temp_list_4 = []
    temp_list_5 = []
    
    for iMode in range (len(WCUT)):
        temp_list_1.append(TGraphErrors())
        temp_list_2.append(TGraphErrors())
        temp_list_3.append(TGraphErrors())
        temp_list_4.append(TGraphErrors())
        temp_list_5.append(TGraphErrors())
        
    gFracClustered_vsRes.append(temp_list_1)
    gFracTrueMatch_vsRes.append(temp_list_2)
    gFracFakeMatch_vsRes.append(temp_list_3)
    gFracTrueExp_vsRes.append(temp_list_4)
    gAge_vsRes.append(temp_list_5)
    


                  

for iSample in range(len(sample)):
    print("sample loop[%s]"%str(iSample))
    
    for iMode in range(len(WCUT)):
        for iRes in range(len(res)):
            print("res loop[%s]"%str(iRes))
            FILENAME = str('./input/%s__dumped_photons_res%s.txt'% (sample[iSample], res_string[iRes]))
    
        #b_output = blossom_matching(FILENAME,M_TRUE, sigma_cut, TOLERANCE, res[iRes], acut_optimal[iRes], bcut, maxEvents)                    
            b_output = blossom_matching(FILENAME,M_TRUE, sigma_cut, TOLERANCE, res[iRes], 1, 1, maxEvents, WCUT[iMode])                    
            
            gFracClustered_vsRes[iSample][iMode].SetPoint(iRes, res[iRes], float(b_output[0]) ) 
            gFracTrueMatch_vsRes[iSample][iMode].SetPoint(iRes, res[iRes], float(b_output[1])) 
            gFracFakeMatch_vsRes[iSample][iMode].SetPoint(iRes, res[iRes], float(b_output[2])) 
            gFracTrueExp_vsRes[iSample][iMode].SetPoint(iRes, res[iRes], float(b_output[3]) )
        
            gAge_vsRes[iSample][iMode].SetPoint(iRes, res[iRes], b_output[6])
                        

                
    
cFracClustered_vsRes = TCanvas("cFracClustered_vsRes", "cFracClustered_vsRes", 500, 500)
gFracClustered_vsRes[0][0].Draw("ALPE")
gFracClustered_vsRes[0][0].GetXaxis().SetLimits(0, 0.3)
gFracClustered_vsRes[0][0].GetXaxis().SetTitle("EM resolution")
gFracClustered_vsRes[0][0].GetYaxis().SetTitle("Fraction of photons")
gFracClustered_vsRes[0][0].SetLineColor(kBlue+1)
gFracClustered_vsRes[0][0].SetMinimum(0)
gFracClustered_vsRes[0][0].SetMaximum(1.2)
gFracClustered_vsRes[0][0].SetLineWidth(2)

gFracTrueMatch_vsRes[0][0].Draw("same LPE")
gFracTrueMatch_vsRes[0][0].SetLineColor(kGreen+1)
gFracTrueMatch_vsRes[0][0].SetLineWidth(2)

gFracFakeMatch_vsRes[0][0].Draw("same LPE")
gFracFakeMatch_vsRes[0][0].SetLineColor(kRed+1)
gFracFakeMatch_vsRes[0][0].SetLineWidth(2)

gFracTrueExp_vsRes[0][0].Draw("same LPE")
gFracTrueExp_vsRes[0][0].SetLineColor(kBlack)
gFracTrueExp_vsRes[0][0].SetLineWidth(2)

if (len(WCUT)>1) :
    gFracClustered_vsRes[0][1].Draw("same LPE")
    #gFracClustered_vsRes[0][1].GetXaxis().SetLimits(0, 0.3)
    gFracClustered_vsRes[0][1].SetLineColor(kBlue)
    gFracClustered_vsRes[0][1].SetLineStyle(7)
    gFracClustered_vsRes[0][1].SetLineWidth(2)
    #gFracClustered_vsRes[0][1].SetMinimum(0)
    #gFracClustered_vsRes[0][1].SetMaximum(1.2)

    gFracTrueMatch_vsRes[0][1].Draw("same LPE")
    gFracTrueMatch_vsRes[0][1].SetLineColor(kGreen)
    gFracTrueMatch_vsRes[0][1].SetLineStyle(7)
    gFracTrueMatch_vsRes[0][1].SetLineWidth(2)

    gFracFakeMatch_vsRes[0][1].Draw("same LPE")
    gFracFakeMatch_vsRes[0][1].SetLineColor(kRed)
    gFracFakeMatch_vsRes[0][1].SetLineStyle(7)
    gFracFakeMatch_vsRes[0][1].SetLineWidth(2)

    gFracTrueExp_vsRes[0][1].Draw("same LPE")
    gFracTrueExp_vsRes[0][1].SetLineColor(kBlack)
    gFracTrueExp_vsRes[0][1].SetLineStyle(7)
    gFracTrueExp_vsRes[0][1].SetLineWidth(2)


leg2 = TLegend(0.5,0.15,0.88,0.4,"","brNDC");
leg2.AddEntry(gFracClustered_vsRes[0][0], "Total clustered", "lp");          
leg2.AddEntry(gFracTrueMatch_vsRes[0][0], "Correct match", "lp");          
leg2.AddEntry(gFracFakeMatch_vsRes[0][0], "Fake match", "lp");          
leg2.AddEntry(gFracTrueExp_vsRes[0][0], "Total real", "lp");          
leg2.Draw();

gPad.SetGridy()
  
cFracClustered_vsRes.Update()



cAge_vsRes = TCanvas("cAge_vsRes", "cAge_vsRes", 500, 500)
gAge_vsRes[0][0].Draw("ALPE")
gAge_vsRes[0][0].GetXaxis().SetLimits(0, 0.3)
gAge_vsRes[0][0].GetXaxis().SetTitle("EM resolution")
gAge_vsRes[0][0].GetYaxis().SetTitle("Computational time / event [s]")
gAge_vsRes[0][0].SetLineColor(kGreen+1)
gAge_vsRes[0][0].SetMinimum(0)
gAge_vsRes[0][0].SetMaximum(0.12)
gAge_vsRes[0][0].SetLineWidth(2)

if (len(WCUT)>1) :
    gAge_vsRes[0][1].Draw("same LPE")
    gAge_vsRes[0][1].SetLineColor(kRed+1)
    gAge_vsRes[0][1].SetLineWidth(2)

leg = TLegend(0.5,0.72,0.88,0.88,"","brNDC");
leg.AddEntry(gAge_vsRes[0][0], "w/ structure", "lp");     
if (len(WCUT)>1) : leg.AddEntry(gAge_vsRes[0][1], "w/o structure", "lp");          
leg.Draw();

gPad.SetGridy()
  
cAge_vsRes.Update()




input('Press ENTER to exit')
