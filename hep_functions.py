import pandas as pd
import numpy as np
import networkx as nx
import itertools
import time
import os, sys
import ROOT
import math
from ROOT import TCanvas, TGraphErrors, TLegend, TH1F, gPad, gStyle, TFile, TH2F

M_TRUE = 0.134976      # invariant mass of the matched events


#initialize edges
def initialize (edges):
    edges['e1'] = edges.node1.copy()
    edges['e2'] = edges.node2.copy()
    edges['px1'] = edges.node1.copy()
    edges['px2'] = edges.node2.copy()
    edges['py1'] = edges.node1.copy()
    edges['py2'] = edges.node2.copy()
    edges['pz1'] = edges.node1.copy()
    edges['pz2'] = edges.node2.copy()
    edges['mother1'] = edges.node1.copy()
    edges['mother2'] = edges.node2.copy()            
    
    return edges
    
    
#assign values of photons to edge nodes
def set_edge_values (edges, energy, momentum, mothers):
    edges = edges.replace({'e1':energy,'e2':energy,
                           'px1':momentum['px'],'px2':momentum['px'],
                           'py1':momentum['py'],'py2':momentum['py'],
                           'pz1':momentum['pz'],'pz2':momentum['pz'],
                           'mother1':mothers,'mother2':mothers})
    
    return edges
    

#return invariant mass of the photon pair
def get_inv_mass (edges):
    p1L2 = np.sqrt(np.abs(edges.px1)**2+np.abs(edges.py1)**2+np.abs(edges.pz1)**2)
    p2L2 = np.sqrt(np.abs(edges.px2)**2+np.abs(edges.py2)**2+np.abs(edges.pz2)**2)
    aij = np.arccos(round((edges.px1*edges.px2+edges.py1*edges.py2+edges.pz1*edges.pz2)/(p1L2*p2L2),8))
    M = np.sqrt(np.abs(4*edges.e1*edges.e2*np.abs(np.sin(aij/2)**2)))
    
    return M



#return boost of the reconstructed photon pair
def get_boost (edges):
    ePi0  = edges.e1+edges.e2
    BOOST = ePi0/get_inv_mass(edges)
    
    return BOOST
    
    
    
#return angle between photons in the rest frame of the reconstructed pi0
def get_angle (edges):
    px_Pi0   = edges.px1+edges.px2
    py_Pi0   = edges.py1+edges.py2
    pz_Pi0   = edges.pz1+edges.pz2
    ePi0     = edges.e1+edges.e2
    
    b_x    = -px_Pi0 / ePi0
    b_y    = -py_Pi0 / ePi0
    b_z    = -pz_Pi0 / ePi0            
    b2     = b_x*b_x + b_y*b_y + b_z*b_z                  
    gamma  = 1.0 / np.sqrt(np.abs(1.0-b2))         
    gamma2 = (gamma - 1.0)/b2
    
    bp_1 = b_x*edges.px1 + b_y*edges.py1 + b_z*edges.pz1;                                                                        
    bp_2 = b_x*edges.px2 + b_y*edges.py2 + b_z*edges.pz2;
    
    p1x_b = edges.px1 + gamma2*bp_1*b_x + gamma*b_x*edges.e1
    p2x_b = edges.px2 + gamma2*bp_2*b_x + gamma*b_x*edges.e2
    p1y_b = edges.py1 + gamma2*bp_1*b_y + gamma*b_y*edges.e1
    p2y_b = edges.py2 + gamma2*bp_2*b_y + gamma*b_y*edges.e2
    p1z_b = edges.pz1 + gamma2*bp_1*b_z + gamma*b_z*edges.e1
    p2z_b = edges.pz2 + gamma2*bp_2*b_z + gamma*b_z*edges.e2
        
    p1B2  = np.sqrt(np.abs(p1x_b)**2+np.abs(p1y_b)**2+np.abs(p1z_b)**2)
    p2B2  = np.sqrt(np.abs(p2x_b)**2+np.abs(p2y_b)**2+np.abs(p2z_b)**2)
    pPi02 = np.sqrt(np.abs(px_Pi0)**2+np.abs(py_Pi0)**2+np.abs(pz_Pi0)**2)
    
    
    ANGLE = np.abs( ( p1x_b*px_Pi0 + p1y_b*py_Pi0 + p1z_b*pz_Pi0)  / (p1B2*pPi02) )
    
    return ANGLE
    
    
    

def get_weight (edges):
    chisquare = np.abs(edges["M"]-M_TRUE)**2/M_TRUE
    WEIGHT = 1-chisquare/chisquare.max()
    #print("weight=" + str(WEIGHT))
    
    return WEIGHT



def get_pdf_weight (edges, res):
    
    input_pdf  = TFile("my_pdf.root", "READ")
    gMass_All  = input_pdf.Get("gMass_All_"+str(res))
    gMass_True = input_pdf.Get("gMass_True_"+str(res))
    gMass_Fake = input_pdf.Get("gMass_Fake_"+str(res))
    
    gAngle_All  = input_pdf.Get("gAngle_All_"+str(res))
    gAngle_True = input_pdf.Get("gAngle_True_"+str(res))
    gAngle_Fake = input_pdf.Get("gAngle_Fake_"+str(res))
    
    gBoost_All  = input_pdf.Get("gBoost_All_"+str(res))
    gBoost_True = input_pdf.Get("gBoost_True_"+str(res))
    gBoost_Fake = input_pdf.Get("gBoost_Fake_"+str(res))

    p_mass = []
    p_angle = []
    p_boost = []
    p_tot = []
    
    for m in edges["M"]:        
        if (gMass_Fake.Eval(m) != 0): p_m = gMass_True.Eval(m) /  gMass_Fake.Eval(m)     *1
        else:                        p_m = 0
        p_mass.append(p_m)

        
    for a in edges["ANGLE"]:        
        if (gAngle_Fake.Eval(a) != 0): p_a = gAngle_True.Eval(a) /  gAngle_Fake.Eval(a)     *1
        else:                         p_a = 0
        p_angle.append(p_a)
    
    for b in edges["BOOST"]:        
        if (gBoost_Fake.Eval(b) != 0): p_b = gBoost_True.Eval(b) /  gBoost_Fake.Eval(b)     *1
        else:                         p_b = 0
        p_boost.append(p_b)
        
    for i in range(len(p_boost)):        
        p_tot.append(p_boost[i] + p_angle[i] + p_mass[i])
        
    WEIGHT = p_tot
        
    return WEIGHT


    
