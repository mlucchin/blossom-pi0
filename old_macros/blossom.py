import pandas as pd
import numpy as np
import networkx as nx
import itertools
import time
import os, sys
import ROOT
import math

def blossom_matching(FILENAME,M_TRUE,SIGMAS=3,TOLERANCE=0.001,E_RES=0.3, ACUT = 1, BCUT = 1, SUP_NODE_NUMBER=10000,maxEvents=10000000,PRECISION=8, return_weights = False):
    '''
    INPUT:
        FILENAME = directory and filename of the event to match
        M_TRUE = invariant mass of the process to be matched
        DEFAULT_INPUTS:
            SIGMAS = 3             # SIGMAS*TOLERANCE = minimum cutoff
            TOLERANCE = 0.001      # SIGMAS*TOLERANCE = minimum cutoff
            E_RES = 0.3            # SIGMAS*(TOLERANCE+E_RES) = cutoff value
            SUP_NODE_NUMBER = 1000 # superior of node number (per event)
            FILENAME = ''          # string directory and filename
            PRECISION = 12         # round acos argument to 12 significant numbers after comma
            return_weights = False # if true returns a list of lists containing matches counts and matches gamma masses:
                                     weights (weights of non matching gammas);
                                     matched_weights (weights of non matched gammas);
                                     mother_weights (weights of non true matching gammas - i.e. with same mother_id);
                                     dupli_weights (weights of gammas left as single - i.e.matched with their copy);
                                     iweights,imatched_weights,imother_weights (the rescaled masses -> from [0-1] being 0 a perfect match);
                                     true_matches_count;
                                     false_matches_count;
                                     dupli_matches_count;
                                     pi0ingammgamma_events_count;
                                     event_count (total number of unique mother_ids per event)
    OUTPUT:
        RETURNS matches (list of lists): each element in matches is a list of tuples; each tuple contains the node_id of the matched gamma
    '''
    
    # Matches are recorded in a list of list of 2-tuple
    matches = []
    ### Load Data
    # blurred inv_mass ~ N(0,E_RES)
    ev0 = pd.read_csv(f'{FILENAME}',names=['event_id','mother_id','px','py','pz','energy'],sep=' ',index_col=False).sort_values('event_id')
    # group by event number
    group0 = ev0.groupby('event_id')
    TOT_EVENTS = len(group0)
    print("Number of events: " + str(TOT_EVENTS))
    print("SCUT: " + str(E_RES) + ", ACUT: " + str(ACUT) + ", BCUT: " + str(BCUT) + ", maxEvents: " + str(maxEvents) )
    EV_NUM = max(ev0.event_id)
    if (maxEvents<TOT_EVENTS): TOT_EVENTS = maxEvents

    if return_weights:
        # save weight values for assessing weight discriminating power
        weights = []
        matched_weights = []
        mother_weights = []
        dupli_weights = []
        iweights = []
        imatched_weights = []
        imother_weights = []
        
    # count matches 
    true_matches_count = []
    false_matches_count = []
    dupli_matches_count = []
    event_count = []
    pi0ingammgamma_events_count = []
    
    frac_trueGammaPi0 = []
    frac_true = []
    frac_fake = []
    frac_tot  = []
    
    all_angle_list = []
    matched_angle_list = []
    all_boost_list = []
    matched_boost_list = []
    all_mass_list = []
    matched_mass_list = []
    
    
    clusteredPi0               = [] 
    unClusteredPhotons         = []
    clusteredPi0_withDaughters = [] 
    
    start = time.time()
    #######################
    for i, event in group0:
        if (i<maxEvents):
        #if (True):
            print(f"Processing events... event number {i} out of {EV_NUM}",end='\r')
            edges = pd.DataFrame(data = list(itertools.combinations(event.index,2))+list(zip(event.index,event.index)), columns = ['node1','node2'])
            edges['layer'] = 0
            layer1 = edges[edges.node1!=edges.node2].copy()
            layer1['layer'] = 1
            edges = edges.append(layer1,sort=True).reset_index(drop=True)

            energy = event['energy'].to_dict()
            momentum = event[['px','py','pz']].to_dict()
            mothers = event['mother_id']
            n_event = event['event_id']
            #n_event = temp.mean
            #print (n_event)

            # assigning node attributes to edges dataframe
            edges['e1'] = edges.node1.copy()
            edges['e2'] = edges.node2.copy()
            edges['px1'] = edges.node1.copy()
            edges['px2'] = edges.node2.copy()
            edges['py1'] = edges.node1.copy()
            edges['py2'] = edges.node2.copy()
            edges['pz1'] = edges.node1.copy()
            edges['pz2'] = edges.node2.copy()
            edges['mother1'] = edges.node1.copy()
            edges['mother2'] = edges.node2.copy()
            edges['node1'] = edges.node1+edges.layer*SUP_NODE_NUMBER
            edges['node2'] = edges.node2+edges.layer*SUP_NODE_NUMBER
            edges.loc[edges['node1']==edges['node2'],'layer'] += 1/2 
            edges.loc[edges['node1']==edges['node2'],'node2'] += SUP_NODE_NUMBER 
            edges = edges.replace({'e1':energy,'e2':energy,
                                'px1':momentum['px'],'px2':momentum['px'],
                                'py1':momentum['py'],'py2':momentum['py'],
                                'pz1':momentum['pz'],'pz2':momentum['pz'],
                                'mother1':mothers,'mother2':mothers})

            ## Computing weights
            edges = edges.sort_values(['node1','node2'])
            p1L2 = np.sqrt(np.abs(edges.px1)**2+np.abs(edges.py1)**2+np.abs(edges.pz1)**2)
            p2L2 = np.sqrt(np.abs(edges.px2)**2+np.abs(edges.py2)**2+np.abs(edges.pz2)**2)
            aij = np.arccos(round((edges.px1*edges.px2+edges.py1*edges.py2+edges.pz1*edges.pz2)/(p1L2*p2L2),PRECISION))
            M = np.sqrt(np.abs(4*edges.e1*edges.e2*np.abs(np.sin(aij/2)**2)))
            
            
            
            px_Pi0   = edges.px1+edges.px2
            py_Pi0   = edges.py1+edges.py2
            pz_Pi0   = edges.pz1+edges.pz2
            ePi0     = edges.e1+edges.e2
            
            b_x    = -px_Pi0 / ePi0
            b_y    = -py_Pi0 / ePi0
            b_z    = -pz_Pi0 / ePi0            
            b2     = b_x*b_x + b_y*b_y + b_z*b_z      
            #print ("b2 = " +str(b2))
            gamma  = 1.0 / np.sqrt(np.abs(1.0-b2))         
            #for i in range (len (gamma) ) : 
                #if (b2.item(i)>0): gamma2.item(i) = (gamma.item(i) - 1.0)/b2.item(i)

            gamma2 = (gamma - 1.0)/b2
            
            bp_1 = b_x*edges.px1 + b_y*edges.py1 + b_z*edges.pz1;                                                                        
            bp_2 = b_x*edges.px2 + b_y*edges.py2 + b_z*edges.pz2;
            
            p1x_b = edges.px1 + gamma2*bp_1*b_x + gamma*b_x*edges.e1
            p2x_b = edges.px2 + gamma2*bp_2*b_x + gamma*b_x*edges.e2
            p1y_b = edges.py1 + gamma2*bp_1*b_y + gamma*b_y*edges.e1
            p2y_b = edges.py2 + gamma2*bp_2*b_y + gamma*b_y*edges.e2
            p1z_b = edges.pz1 + gamma2*bp_1*b_z + gamma*b_z*edges.e1
            p2z_b = edges.pz2 + gamma2*bp_2*b_z + gamma*b_z*edges.e2
                        
                        
            p1B2  = np.sqrt(np.abs(p1x_b)**2+np.abs(p1y_b)**2+np.abs(p1z_b)**2)
            p2B2  = np.sqrt(np.abs(p2x_b)**2+np.abs(p2y_b)**2+np.abs(p2z_b)**2)
            pPi02 = np.sqrt(np.abs(px_Pi0)**2+np.abs(py_Pi0)**2+np.abs(pz_Pi0)**2)
            
            
            #p1B2  = np.sqrt(np.abs(edges.px1-px_Pi0)**2+np.abs(edges.py1-py_Pi0)**2+np.abs(edges.pz1-pz_Pi0)**2)
            #p2B2  = np.sqrt(np.abs(edges.px2-px_Pi0)**2+np.abs(edges.py2-py_Pi0)**2+np.abs(edges.pz2-pz_Pi0)**2)
            #pPi02 = np.sqrt(np.abs(px_Pi0)**2+np.abs(py_Pi0)**2+np.abs(pz_Pi0)**2)
                                    
            ANGLE = np.abs( ( p1x_b*px_Pi0 + p1y_b*py_Pi0 + p1z_b*pz_Pi0)  / (p1B2*pPi02) )            
            #ANGLE = np.abs( ( (edges.px1-px_Pi0)*px_Pi0 + (edges.py1-py_Pi0)*py_Pi0 + (edges.pz1-pz_Pi0)*pz_Pi0)  /(p1B2*pPi02) )            
            all_angle_list.append(ANGLE)
            
            BOOST = ePi0/M
            all_boost_list.append(ePi0/M)
            all_mass_list.append(M)
            
            
            
            #control plots with true edges
            true_edges = edges.copy()
            true_edges = true_edges[(true_edges.mother1==true_edges.mother2) & (true_edges.layer==0)]
            matched_p1L2 = np.sqrt(np.abs(true_edges.px1)**2+np.abs(true_edges.py1)**2+np.abs(true_edges.pz1)**2)
            matched_p2L2 = np.sqrt(np.abs(true_edges.px2)**2+np.abs(true_edges.py2)**2+np.abs(true_edges.pz2)**2)
            matched_aij = np.arccos(round((true_edges.px1*true_edges.px2+true_edges.py1*true_edges.py2+true_edges.pz1*true_edges.pz2)/(matched_p1L2*matched_p2L2),PRECISION))
            matched_M = np.sqrt(np.abs(4*true_edges.e1*true_edges.e2*np.abs(np.sin(matched_aij/2)**2)))
            
            matched_px_Pi0   = true_edges.px1+true_edges.px2
            matched_py_Pi0   = true_edges.py1+true_edges.py2
            matched_pz_Pi0   = true_edges.pz1+true_edges.pz2
            matched_E_Pi0    = true_edges.e1+true_edges.e2
            
            b_x    = -matched_px_Pi0 / matched_E_Pi0
            b_y    = -matched_py_Pi0 / matched_E_Pi0
            b_z    = -matched_pz_Pi0 / matched_E_Pi0            
            b2     = b_x*b_x + b_y*b_y + b_z*b_z                                                
            gamma  = 1.0 / np.sqrt(np.abs(1.0-b2))         
            gamma2 = (gamma - 1.0)/b2

                        
            bp_1 = b_x*true_edges.px1 + b_y*true_edges.py1 + b_z*true_edges.pz1;                                                                        
            bp_2 = b_x*true_edges.px2 + b_y*true_edges.py2 + b_z*true_edges.pz2;                                                                        
            
            m_p1x_b = true_edges.px1 + gamma2*bp_1*b_x + gamma*b_x*true_edges.e1
            m_p2x_b = true_edges.px2 + gamma2*bp_2*b_x + gamma*b_x*true_edges.e2
            m_p1y_b = true_edges.py1 + gamma2*bp_1*b_y + gamma*b_y*true_edges.e1
            m_p2y_b = true_edges.py2 + gamma2*bp_2*b_y + gamma*b_y*true_edges.e2
            m_p1z_b = true_edges.pz1 + gamma2*bp_1*b_z + gamma*b_z*true_edges.e1
            m_p2z_b = true_edges.pz2 + gamma2*bp_2*b_z + gamma*b_z*true_edges.e2
            
            #print("ratio: " + str(m_p1x_b/m_p2x_b))
            #print("ratio: " + str(m_p1y_b/m_p2y_b))
            #print("ratio: " + str(m_p1z_b/m_p2z_b))
            
            
            matched_p1B2  = np.sqrt(np.abs(m_p1x_b)**2+np.abs(m_p1y_b)**2+np.abs(m_p1z_b)**2)
            matched_p2B2  = np.sqrt(np.abs(m_p2x_b)**2+np.abs(m_p2y_b)**2+np.abs(m_p2z_b)**2)
            matched_pPi02 = np.sqrt(np.abs(matched_px_Pi0)**2+np.abs(matched_py_Pi0)**2+np.abs(matched_pz_Pi0)**2)
                                    
            matched_ANGLE = np.abs( ( m_p1x_b*matched_px_Pi0 + m_p1y_b*matched_py_Pi0 + m_p1z_b*matched_pz_Pi0)  /(matched_p1B2*matched_pPi02) )            
            matched_angle_list.append(matched_ANGLE)
            
            
            matched_boost_list.append(matched_E_Pi0/matched_M)
            matched_mass_list.append(matched_M)
            
            
            
            
            
            
            edges['weight'] = np.abs(M-M_TRUE)**2/M_TRUE
            #edges['weight'] = np.abs(M-M_TRUE)**2/M_TRUE + 1/BOOST**4
            #edges['weight'] = np.abs(M-M_TRUE)/M_TRUE
            
            #edges['angle'] = ANGLE
            edges['inverse_weight'] = np.abs(edges.weight/edges.weight.max()-1)
            #edges['inverse_weight'] = np.abs(1./edges.weight)
            edges['e_tup'] = [tuple(sorted(i)) for i in list(zip(edges.node1.values,edges.node2.values))]
    
    
            this_fracPhotonsFromPi0 = float(len(edges[(edges.mother1==edges.mother2)&(edges.layer==0)])*2 ) / float(len(event))
            frac_trueGammaPi0.append(this_fracPhotonsFromPi0)
            totNPhotons = float(len(event))
            #print("fracPhotonsFromPi0 = " + str(this_fracPhotonsFromPi0) + " -- totNPhotons = " + str(totNPhotons) )
            
            
            # chi-squared cut-off (invariant mass squared difference cut-off)
            cut_off = SIGMAS*(E_RES)+TOLERANCE
            #cut_off = TOLERANCE
            #edges = edges[edges.weight<=cut_off]
            #edges = edges[np.abs(M-M_TRUE)/M_TRUE<=cut_off]
            
            #print ("ANGLE = "  +str(ANGLE))
            edges = edges[(np.abs(M-M_TRUE)/M_TRUE<=cut_off) & (ANGLE<=ACUT) & (BOOST>=BCUT)]
            #edges = edges[(np.abs(M-M_TRUE)/M_TRUE<=cut_off)]
            #edges = edges[ANGLE<=ACUT]
            
            #edges = edges[(np.abs(M-M_TRUE)/M_TRUE<=cut_off) & (edges.layer==0)]
            #edges = edges[(np.sqrt(edges.weight)<=cut_off*M_TRUE)&(edges.layer==0)]
            
            
            this_fracPhotonsFromPi0 = float(len(edges[(edges.mother1==edges.mother2)&(edges.layer==0)])*2 ) / float(len(event))
            #frac_trueGammaPi0.append(this_fracPhotonsFromPi0)
            #print("fracPhotonsFromPi0 post Cut = " + str(this_fracPhotonsFromPi0) )
            
            #######################
            ### Build graph
            G = nx.from_pandas_edgelist(edges[['node1','node2','inverse_weight','mother1','mother2']],'node1','node2',edge_attr=['inverse_weight','mother1','mother2'])
            ### compute best matches following blossom algorithm 
            # (https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.matching.max_weight_matching.html)
            matched = [tuple(sorted(i)) for i in list(nx.algorithms.matching.max_weight_matching(G,weight='inverse_weight'))]
            #######################
    
            
            matched_photons = list(edges[(edges.e_tup.isin(matched))&(edges.layer==0)].e_tup)
            unmatched_photons = list(edges[(edges.e_tup.isin(matched))&(edges.layer==1/2)].node1)
                        
            spare_photons = [photon for photon in event.index if photon not in list(sum(matched_photons, ()))]
            #print(str(matched_photons))

            #MATCHES_ATTRIBUTES = from_matched_to_attributes(matched_photons,i)
            a = from_matched_to_attributes(matched_photons,event)
            
            APPR = 11
            #build collection of clustered pi0 w/ w/o daughters and of spare photons 
            for i_pair in range(len(a)):                
                p1_x = a[i_pair][list(a[i_pair].keys())[0]]['px']                
                p1_y = a[i_pair][list(a[i_pair].keys())[0]]['py']                
                p1_z = a[i_pair][list(a[i_pair].keys())[0]]['pz']                
                e1   = a[i_pair][list(a[i_pair].keys())[0]]['energy']                            
                p2_x = a[i_pair][list(a[i_pair].keys())[1]]['px']                
                p2_y = a[i_pair][list(a[i_pair].keys())[1]]['py']                
                p2_z = a[i_pair][list(a[i_pair].keys())[1]]['pz']                
                e2   = a[i_pair][list(a[i_pair].keys())[1]]['energy']            
                this_pair = [a[i_pair][list(a[i_pair].keys())[1]]['event_id'], round(p1_x,APPR), round(p1_y,APPR), round(p1_z,APPR), round(e1,APPR), round(p2_x,APPR), round(p2_y,APPR), round(p2_z,APPR), round(e2,APPR)]
                clusteredPi0_withDaughters.append(this_pair)
                
            b = from_unmatched_to_attributes(spare_photons,event)    
            for i_g in b.keys():
                p1_x = b[i_g]['px']                
                p1_y = b[i_g]['py']  
                p1_z = b[i_g]['pz']  
                e1 = b[i_g]['energy']                  
                this_gamma = [b[i_g]['event_id']  , round(p1_x,APPR), round(p1_y,APPR), round(p1_z,APPR), round(e1,APPR)]
                unClusteredPhotons.append(this_gamma)
            

            #if ((len(a)*2 + len(b))/ float(len(event)) == 1): print ("photon counting is ok --> " + str((len(a)*2 + len(b))/ float(len(event)) ) )
            #else:                                             
            #print ("ERROR photon counting not ok! --> (" + str(len(a)*2) + " + " + str(len(b)) + ") / " +str(len(event)) + " = " + str((len(a)*2 + len(b))/ float(len(event))))
            
            
            # count matches for event x
            #
            this_ntrue_pairs = len(edges[((edges.e_tup.isin(matched)))&(edges.mother1==edges.mother2)&(edges.layer==0)  ])            
            true_matches_count.append(this_ntrue_pairs)
            
            this_nfake_pairs = len(edges[((edges.e_tup.isin(matched)))&(edges.mother1!=edges.mother2)&(edges.layer==0)])                        
            false_matches_count.append(this_nfake_pairs)            
            
            dupli_matches_count.append(len(edges[((edges.e_tup.isin(matched)))&(edges.layer==1/2)]))            
            event_count.append(len(event.drop_duplicates('mother_id')))                        
            pi0ingammgamma_events_count.append(len(edges[(edges.mother1==edges.mother2)&(edges.layer==0)]))
            
            
            frac_true.append( float(this_ntrue_pairs*2./totNPhotons))
            frac_fake.append( float(this_nfake_pairs*2./totNPhotons))
            frac_tot.append ( float((this_ntrue_pairs+this_nfake_pairs)*2./totNPhotons))
            
            #print ("frac_true = " + str(float(this_ntrue_pairs*2/totNPhotons)) + "frac_fake = " + str(float(this_nfake_pairs*2/totNPhotons)) + "frac_tot = " + str(float((this_ntrue_pairs+this_nfake_pairs)*2/totNPhotons)) )
            
            
            #frac_pi0ingammgamma_events_count.append(len(edges[(edges.mother1==edges.mother2)&(edges.layer==0)])*2 / len(event))
    
            if return_weights:
                # save weights 
                weights.append(edges[(edges.mother1!=edges.mother2)&(edges.layer==0)].weight)
                matched_weights.append(edges[(edges.e_tup.isin(matched))&(edges.layer==0)].weight)
                mother_weights.append(edges[(edges.mother1==edges.mother2)&(edges.layer==0)].weight)
                dupli_weights.append(edges[(edges.e_tup.isin(matched))&(edges.layer==1/2)].weight)
                # save inverse_weights 
                iweights.append(edges.inverse_weight)
                imatched_weights.append(edges[(edges.e_tup.isin(matched))&(edges.layer==0)].inverse_weight)
                imother_weights.append(edges[(edges.mother1==edges.mother2)&(edges.layer==0)].inverse_weight)
                
            matches.append(list(edges[(edges.e_tup.isin(matched))&(edges.layer==0)].e_tup))
            
    end = time.time()
    print('Processing complete!')
    print('AGE: ',end - start)
    
    
    N_TRUE  = float(np.sum(np.array(frac_true)) /len(frac_true))
    N_FALSE = float(np.sum(np.array(frac_fake)) /len(frac_true))
    N_ALL   = float(np.sum(np.array(frac_tot))  /len(frac_true))
    N_EXP   = float(np.sum(np.array(frac_trueGammaPi0))  /len(frac_trueGammaPi0))
    
    print(f'Matches found: {N_TRUE+N_FALSE} [{N_TRUE} True-positive and {N_FALSE} False-positive] out of {N_EXP} in {TOT_EVENTS} events')
    
    summary = [N_ALL, N_TRUE, N_FALSE, N_EXP, all_angle_list, matched_angle_list, all_boost_list, matched_boost_list, all_mass_list, matched_mass_list, clusteredPi0_withDaughters, unClusteredPhotons]
        
    if return_weights:
        return matches,[weights,matched_weights,mother_weights,dupli_weights,iweights,imatched_weights,imother_weights,true_matches_count,false_matches_count,dupli_matches_count,pi0ingammgamma_events_count,event_count]
    else:
        #return matches
        return summary
    
    
    
    
    
    
    
    
    
    
def from_matched_to_attributes(MATCHED,EVENT):
    """ 
        The idea is super-simple:
        EVENT.loc[photon1].to_dict() take the EVENT DataFrame 
        and select the attibutes of the "photon"
        RETURNS: a list of dicts (containing 2 photon names and each name a dict of attributes)
    """
    attributes = []
    for photon1,photon2 in MATCHED:
        attributes.append(EVENT.loc[[photon1,photon2]].T.to_dict())
    return attributes

def from_unmatched_to_attributes(UNMATCHED,EVENT):
    return EVENT.loc[UNMATCHED].T.to_dict()


def truncate(number, decimals=0):
    """
    Returns a value truncated to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer.")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more.")
    elif decimals == 0:
        return math.trunc(number)

    factor = 10.0 ** decimals
    return math.trunc(number * factor) / factor
  
    
