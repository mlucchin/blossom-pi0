import os, sys
from blossom import blossom_matching


  

res_string = ["0.0", "0.01", "0.03", "0.05", "0.1", "0.15", "0.3"]

sample = [ "hzz_bbqqqq", "hbb_znunu", "hbb_zqq",]

sigma_cut = [3]

acut = [0.8, 0.90, 0.95, 0.97, 0.98, 0.99, 1.0]
bcut = [1, 1.5, 2, 3, 5, 10, 15, 20, 50]

maxEvents = 100


if (len(sys.argv)>1): maxEvents=int(sys.argv[1])
if (len(sys.argv)>2): sample=[str(sys.argv[2])]
if (len(sys.argv)>3): sigma_cut=[float(sys.argv[3])]
if (len(sys.argv)>4): acut=[float(sys.argv[4])]
if (len(sys.argv)>5): bcut=[float(sys.argv[5])]
if (len(sys.argv)>6): res_string=[str(sys.argv[6])]

print("Max N of Events: " + str(maxEvents))
print("Sample type: " + str(sample))
print("Sigma mass cut: " + str(sigma_cut))
print("Cut on Max Aperture: " + str(acut))
print("Cut Min Boost: " + str(bcut))
print("EM res: " + str(res_string))




files = []


M_TRUE = 0.134976      # invariant mass of the matched events
TOLERANCE = 0.0001      # SIGMAS*TOLERANCE = minimum cutoff        
SUP_NODE_NUMBER = 1000 # superior of node number (per event)
return_weights = False


    
    
WRITE_OUTPUT = True


                  

for iSample in range(len(sample)):
    print("sample loop[%s]"%str(iSample))
    for iRes in range(len(res_string)):
        print("res loop[%s]"%str(iRes))
        FILENAME = str('./input/%s__dumped_photons_res%s.txt'% (sample[iSample], res_string[iRes]))
        
        for iSigma in range(len(sigma_cut)):
            
            for iACut in range(len(acut)):    
                
                for iBCut in range(len(bcut)):    
                    print("sample: %s , res_cut: %s, aCut: %s, bCut: %s, sigma_cut: %s"% (sample[iSample], res_string[iRes], str(acut[iACut]),  bcut[iBCut], str(sigma_cut[iSigma]) ) )
                        
                    feedBackBlossom      = open("./output/pairedPhotons_" + sample[iSample] + "_" + res_string[iRes] + "_acut_" + str(acut[iACut]) + "_bcut_" + str(bcut[iBCut]) + ".txt", "w")
                    feedBackBlossomSpare = open("./output/sparePhotons_"  + sample[iSample] + "_" + res_string[iRes] + "_acut_" + str(acut[iACut]) + "_bcut_" + str(bcut[iBCut]) + ".txt", "w")
                    summaryFile = open("./output/summaryAlgo_"  + sample[iSample] + "_" + res_string[iRes] + "_acut_" + str(acut[iACut]) + "_bcut_" + str(bcut[iBCut]) + ".txt", "w")   
                    
                    b_output = blossom_matching(FILENAME,M_TRUE, sigma_cut[iSigma], TOLERANCE, float(res_string[iRes]), acut[iACut], bcut[iBCut], SUP_NODE_NUMBER, maxEvents)                    
                                        
                    clusteredPhotons = b_output[10]
                    for mypair in clusteredPhotons:                     
                        if (WRITE_OUTPUT): feedBackBlossom.write(str(mypair[0]) + str(" ") + str(mypair[1]) + str(" ") + str(mypair[2]) + str(" ") + str(mypair[3]) + str(" ") + str(mypair[4]) + str(" ") + str(mypair[5]) + str(" ") + str(mypair[6]) + str(" ") + str(mypair[7]) + str(" ") + str(mypair[8]) +  "\n") #write photons info
                    
                    sparePhotons = b_output[11]
                    for myspare in sparePhotons:                     
                        if (WRITE_OUTPUT): feedBackBlossomSpare.write(str(myspare[0]) + str(" ") + str(myspare[1]) + str(" ") + str(myspare[2]) + str(" ") + str(myspare[3]) + str(" ") + str(myspare[4]) + "\n") #write photons info
            
                    if (WRITE_OUTPUT): summaryFile.write(str(b_output[0]) + str(" ") + str(b_output[1]) + str(" ") + str(b_output[2]) + str(" ") + str(b_output[3]) + "\n") 
                    
                    feedBackBlossom.close()
                    feedBackBlossomSpare.close()
                    summaryFile.close()


#input('Press ENTER to exit')
