import os, sys
import ROOT

from ROOT import TCanvas, TGraphErrors, TLegend, TH1F, gPad, gStyle, TFile, TH2F
from ROOT import kGreen, kBlue, kRed, kYellow, kBlack, NULL
from blossom import blossom_matching


gStyle.SetTitleXOffset (1.00) ;                                                                                        
gStyle.SetTitleYOffset (1.2) ;                                                                                                                                                                                                                 
gStyle.SetPadLeftMargin (0.13) ;                                                                                       
gStyle.SetPadBottomMargin (0.13) ;                                                                                                                                                                                                              
gStyle.SetTitleSize (0.05, "xyz") ;                                                                                    
gStyle.SetLabelSize (0.035,"xyz") ;  
    
gStyle.SetLegendBorderSize(0);
gStyle.SetLegendFillColor(0);
gStyle.SetLegendFont(42);
gStyle.SetLegendTextSize(0.035);
#TLegend leg;
  

res_string = ["0.0", "0.01", "0.03", "0.05", "0.1", "0.15", "0.3"]
res        = [0, 0.01, 0.03, 0.05, 0.10, 0.15,  0.3]
acut_optimal = [1, 0.99, 0.98, 0.97 , 0.965, 0.95, 0.9]

#res_string = ["0.0", "0.03", "0.3"]
#res        = [0, 0.03, 0.3]
#acut_optimal = [1, 0.95, 0.90]

#res_string = ["0.03"]
#res        = [0.03]
#acut_optimal = [0.98]

#sample = ["hzz_bbqqqq"]
sample = [ "hzz_bbqqqq", "hbb_znunu", "hbb_zqq",]
#sample = ["hbb_znunu"]
#sample = ["hbb_zqq"]

#sigma_cut = [0.25, 0.5, 1, 2, 3, 4, 6]
sigma_cut = [3]

acut = [0.8, 0.90, 0.95, 0.97, 0.98, 0.99, 1.0]
#acut = [0.98]
bcut = [1, 1.5, 2, 3, 5, 10, 15, 20, 50]
#bcut = [1]



files = []


M_TRUE = 0.134976      # invariant mass of the matched events
TOLERANCE = 0.0001      # SIGMAS*TOLERANCE = minimum cutoff        
SUP_NODE_NUMBER = 1000 # superior of node number (per event)
return_weights = False
maxEvents = 1000


gFracClustered_vsRes = []
gFracTrueMatch_vsRes = []
gFracFakeMatch_vsRes = []
gFracTrueExp_vsRes = []

gFracClustered_vsSigma = []
gFracTrueMatch_vsSigma = []
gFracFakeMatch_vsSigma = []
gFracTrueExp_vsSigma = []

gFracClustered_vsAcut = []
gFracTrueMatch_vsAcut = []
gFracFakeMatch_vsAcut = []
gFracRatioMatch_vsAcut = []
gFracTrueExp_vsAcut   = []

gFracClustered_vsBcut = []
gFracTrueMatch_vsBcut = []
gFracFakeMatch_vsBcut = []
gFracRatioMatch_vsBcut = []
gFracTrueExp_vsBcut   = []

hAngleTest = []
hAngleTestTrue = []
hBoostTest = []
hBoostTestTrue = []
hBoost_vs_Angle = []
hBoost_vs_Angle_True = []
hMassTest = []
hMassTestTrue = []

for iSample in range(len(sample)):
    gFracClustered_vsRes.append(TGraphErrors())
    gFracTrueMatch_vsRes.append(TGraphErrors())
    gFracFakeMatch_vsRes.append(TGraphErrors())
    gFracTrueExp_vsRes.append(TGraphErrors())
    
    temp_1 = []
    temp_2 = []
    temp_3 = []
    temp_4 = []
    temp_4_b = []
    temp_5 = []
    temp_6 = []
    temp_7 = []
    temp_8 = []
    temp_9 = []
    
    for iRes in range(len(res)):
        temp_1.append(TGraphErrors())
        temp_2.append(TGraphErrors())
        temp_3.append(TGraphErrors())
        temp_4.append(TGraphErrors())
        temp_4_b.append(TGraphErrors())
        temp_5.append(TGraphErrors())
        temp_6.append(TGraphErrors())
        temp_7.append(TGraphErrors())
        temp_8.append(TGraphErrors())
        temp_9.append(TGraphErrors())

    gFracClustered_vsAcut.append(temp_1)
    gFracTrueMatch_vsAcut.append(temp_2)
    gFracFakeMatch_vsAcut.append(temp_3)
    gFracTrueExp_vsAcut.append(temp_4)
    gFracRatioMatch_vsAcut.append(temp_4_b)
    
    gFracClustered_vsBcut.append(temp_5)
    gFracTrueMatch_vsBcut.append(temp_6)
    gFracFakeMatch_vsBcut.append(temp_7)
    gFracTrueExp_vsBcut.append(temp_8)
    gFracRatioMatch_vsBcut.append(temp_9)
    
    
WRITE_OUTPUT = False

feedBackBlossom_acut = []
feedBackBlossomSpare_acut = []
feedBackBlossom_bcut = []
feedBackBlossomSpare_bcut = []

for iSample in range(len(sample)):    
    temp_a = []    
    temp_c = []    
    temp_w = []    
    temp_v = []    
    for iRes in range(len(res)):        
        #if (WRITE_OUTPUT): temp_a.append( open("../feedbackBlossom/pairedPhotons_" + sample[iSample] + "_" + res_string[iRes] + ".txt", "w")    )
        #if (WRITE_OUTPUT): temp_c.append( open("../feedbackBlossom/sparePhotons_"  + sample[iSample] + "_" + res_string[iRes] + ".txt", "w")    )
            
        temp_b = []        
        temp_d = []        
        for iACut in range(len(acut)):    
            if (WRITE_OUTPUT): temp_b.append( open("../feedbackBlossom/pairedPhotons_" + sample[iSample] + "_" + res_string[iRes] + "_acut_" + str(acut[iACut]) + ".txt", "w")    )
            if (WRITE_OUTPUT): temp_d.append( open("../feedbackBlossom/sparePhotons_"  + sample[iSample] + "_" + res_string[iRes] + "_acut_" + str(acut[iACut]) + ".txt", "w")    )
        temp_a.append(temp_b)      
        temp_c.append(temp_d)        
        
        temp_x = []        
        temp_y = []        
        for iBCut in range(len(bcut)):    
            if (WRITE_OUTPUT): temp_x.append( open("../feedbackBlossom/pairedPhotons_" + sample[iSample] + "_" + res_string[iRes] + "_bcut_" + str(bcut[iBCut]) + ".txt", "w")    )
            if (WRITE_OUTPUT): temp_y.append( open("../feedbackBlossom/sparePhotons_"  + sample[iSample] + "_" + res_string[iRes] + "_bcut_" + str(bcut[iBCut]) + ".txt", "w")    )
        temp_w.append(temp_x)      
        temp_v.append(temp_y)        
        
    feedBackBlossom_acut.append(temp_a)
    feedBackBlossomSpare_acut.append(temp_c)
    feedBackBlossom_bcut.append(temp_w)
    feedBackBlossomSpare_bcut.append(temp_v)
    




for iRes in range(len(res)):
    gFracClustered_vsSigma.append(TGraphErrors())
    gFracTrueMatch_vsSigma.append(TGraphErrors())
    gFracFakeMatch_vsSigma.append(TGraphErrors())
    gFracTrueExp_vsSigma.append(TGraphErrors())

    
    hAngleTest.append (TH1F("hAngleTest_%s"%res_string[iRes], "hAngleTest_%s"%res_string[iRes], 100, 0, 1))
    hAngleTestTrue.append (TH1F("hAngleTestTrue_%s"%res_string[iRes], "hAngleTestTrue_%s"%res_string[iRes], 100, 0, 1))
    
    hBoostTest.append (TH1F("hBoostTest_%s"%res_string[iRes], "hBoostTest_%s"%res_string[iRes], 500, 0, 100))
    hBoostTestTrue.append (TH1F("hBoostTestTrue_%s"%res_string[iRes], "hBoostTestTrue_%s"%res_string[iRes], 500, 0, 100))
    
    hBoost_vs_Angle.append (TH2F("hBoost_vs_Angle%s"%res_string[iRes], "hBoost_vs_Angle%s"%res_string[iRes], 500, 0, 100, 100, 0, 1))
    hBoost_vs_Angle_True.append (TH2F("hBoost_vs_Angle_True%s"%res_string[iRes], "hBoost_vs_Angle_True%s"%res_string[iRes], 500, 0, 100, 100, 0, 1))
    
    hMassTest.append (TH1F("hMassTest_%s"%res_string[iRes], "hMassTest_%s"%res_string[iRes], 1000, 0, 1))
    hMassTestTrue.append (TH1F("hMassTestTrue_%s"%res_string[iRes], "hMassTestTrue_%s"%res_string[iRes], 1000, 0, 1))
    


                  

for iSample in range(len(sample)):
    print("sample loop[%s]"%str(iSample))
    for iRes in range(len(res)):
        print("res loop[%s]"%str(iRes))
        FILENAME = str('../dumped_pi0_data/%s__dumped_photons_res%s.txt'% (sample[iSample], res_string[iRes]))
    
    
        for iSigma in range(len(sigma_cut)):
            
            for iBCut in range(len(bcut)):    
                
                print("sample: %s , res_cut: %s, aCut: %s, bCut: %s, sigma_cut: %s"% (sample[iSample], res_string[iRes], acut_optimal[iRes],  str(bcut[iBCut]), str(sigma_cut[iSigma]) ) )
                                    
                b_output = blossom_matching(FILENAME,M_TRUE, sigma_cut[iSigma], TOLERANCE, res[iRes], acut_optimal[iRes], bcut[iBCut], SUP_NODE_NUMBER, maxEvents)                    
                
                gFracClustered_vsBcut[iSample][iRes].SetPoint(iBCut, bcut[iBCut], float(b_output[0]) ) 
                gFracTrueMatch_vsBcut[iSample][iRes].SetPoint(iBCut, bcut[iBCut], float(b_output[1])) 
                gFracFakeMatch_vsBcut[iSample][iRes].SetPoint(iBCut, bcut[iBCut], float(b_output[2])) 
                gFracTrueExp_vsBcut[iSample][iRes].SetPoint(iBCut,   bcut[iBCut], float(b_output[3]) )
                gFracRatioMatch_vsBcut[iSample][iRes].SetPoint(iBCut,   bcut[iBCut], float(b_output[2]/b_output[1]) )
                                                
                clusteredPhotons = b_output[10]
                for mypair in clusteredPhotons:                     
                    if (WRITE_OUTPUT): feedBackBlossom_bcut[iSample][iRes][iBCut].write(str(mypair[0]) + str(" ") + str(mypair[1]) + str(" ") + str(mypair[2]) + str(" ") + str(mypair[3]) + str(" ") + str(mypair[4]) + str(" ") + str(mypair[5]) + str(" ") + str(mypair[6]) + str(" ") + str(mypair[7]) + str(" ") + str(mypair[8]) +  "\n") #write photons info
                
                sparePhotons = b_output[11]
                for myspare in sparePhotons:                     
                    if (WRITE_OUTPUT): feedBackBlossomSpare_bcut[iSample][iRes][iBCut].write(str(myspare[0]) + str(" ") + str(myspare[1]) + str(" ") + str(myspare[2]) + str(" ") + str(myspare[3]) + str(" ") + str(myspare[4]) + "\n") #write photons info
            
            
            for iACut in range(len(acut)):    
                print("sample: %s , res_cut: %s, aCut: %s, bCut: %s, sigma_cut: %s"% (sample[iSample], res_string[iRes], str(acut[iACut]),  1, str(sigma_cut[iSigma]) ) )
                                    
                b_output = blossom_matching(FILENAME,M_TRUE, sigma_cut[iSigma], TOLERANCE, res[iRes], acut[iACut], 1, SUP_NODE_NUMBER, maxEvents)                    
                
                gFracClustered_vsAcut[iSample][iRes].SetPoint(iACut, acut[iACut], float(b_output[0]) ) 
                gFracTrueMatch_vsAcut[iSample][iRes].SetPoint(iACut, acut[iACut], float(b_output[1])) 
                gFracFakeMatch_vsAcut[iSample][iRes].SetPoint(iACut, acut[iACut], float(b_output[2])) 
                gFracTrueExp_vsAcut[iSample][iRes].SetPoint(iACut,   acut[iACut], float(b_output[3]) )
                gFracRatioMatch_vsAcut[iSample][iRes].SetPoint(iACut,   acut[iACut], float(b_output[2]/b_output[1]) )
                                
                clusteredPhotons = b_output[10]
                for mypair in clusteredPhotons:                     
                    if (WRITE_OUTPUT): feedBackBlossom_acut[iSample][iRes][iACut].write(str(mypair[0]) + str(" ") + str(mypair[1]) + str(" ") + str(mypair[2]) + str(" ") + str(mypair[3]) + str(" ") + str(mypair[4]) + str(" ") + str(mypair[5]) + str(" ") + str(mypair[6]) + str(" ") + str(mypair[7]) + str(" ") + str(mypair[8]) +  "\n") #write photons info
                    
                sparePhotons = b_output[11]
                for myspare in sparePhotons:                     
                    if (WRITE_OUTPUT): feedBackBlossomSpare_acut[iSample][iRes][iACut].write(str(myspare[0]) + str(" ") + str(myspare[1]) + str(" ") + str(myspare[2]) + str(" ") + str(myspare[3]) + str(" ") + str(myspare[4]) + "\n") #write photons info
            
                    
                    

                    
                        #gFracClustered_vsRes[iSample].SetPoint(iRes, res[iRes], float(b_output[0]) ) 
                        #gFracTrueMatch_vsRes[iSample].SetPoint(iRes, res[iRes], float(b_output[1])) 
                        #gFracFakeMatch_vsRes[iSample].SetPoint(iRes, res[iRes], float(b_output[2])) 
                        #gFracTrueExp_vsRes[iSample].SetPoint(iRes, res[iRes], float(b_output[3]) )
                all_boosts = []
                all_true_boosts = []
                all_angles = []
                all_true_angles = []
                
                
                angle_list = b_output[4]
                for angle in angle_list: 
                    for i in angle:
                        hAngleTest[iRes].Fill(i)
                        all_angles.append(i)
                            
                true_angle_list = b_output[5]
                for angle in true_angle_list: 
                    for i in angle:
                        hAngleTestTrue[iRes].Fill(i)
                        all_true_angles.append(i)
                            
                boost_list = b_output[6]
                for boost in boost_list: 
                    for i in boost:
                        hBoostTest[iRes].Fill(i)
                        all_boosts.append(i)
                            
                true_boost_list = b_output[7]                
                for boost in true_boost_list: 
                    for i in boost:
                        hBoostTestTrue[iRes].Fill(i)
                        all_true_boosts.append(i)
                        
                
                for i in range (len(all_boosts)): 
                    hBoost_vs_Angle[iRes].Fill(all_boosts[i], all_angles[i])
                for i in range (len(all_true_boosts)): 
                    hBoost_vs_Angle_True[iRes].Fill(all_true_boosts[i], all_true_angles[i])
                    
                    #t_boost = true_boost_list[iEv]
                    #t_angle = true_angle_list[iEv]           
                    #print ("iEv = " + str(iEv) + " boost[" + str(0) + "]  = " + str(t_boost[0]))
                    #for i in range (len(t_boost)):                        
                        
                        

                #for iEv in range (len(boost_list)): 
                    #t_boost = boost_list[iEv]
                    #t_angle = angle_list[iEv]                    
                    #for i in range (len(t_boost)):                        
                        #hBoost_vs_Angle[iRes].Fill(t_boost[i], t_angle[i])
                        
                mass_list = b_output[8]
                for mass in mass_list: 
                    for i in mass:
                        hMassTest[iRes].Fill(i)
                            
                true_mass_list = b_output[9]
                for mass in true_mass_list: 
                    for i in mass:
                        hMassTestTrue[iRes].Fill(i)
                    
                    #if (iSample == 0): 
                        #gFracClustered_vsSigma[iRes].SetPoint(iSigma, sigma_cut[iSigma], float(b_output[0]) ) 
                        #gFracTrueMatch_vsSigma[iRes].SetPoint(iSigma, sigma_cut[iSigma], float(b_output[1])) 
                        #gFracFakeMatch_vsSigma[iRes].SetPoint(iSigma, sigma_cut[iSigma], float(b_output[2])) 
                        #gFracTrueExp_vsSigma[iRes].SetPoint(iSigma, sigma_cut[iSigma], float(b_output[3]) ) 
    
    
cFracClustered_vsRes = TCanvas("cFracClustered_vsRes", "cFracClustered_vsRes", 500, 500)
gFracClustered_vsRes[0].Draw("ALPE")
gFracClustered_vsRes[0].SetLineColor(kBlue+1)
gFracClustered_vsRes[0].SetMinimum(0)
gFracClustered_vsRes[0].SetMaximum(1.2)

gFracTrueMatch_vsRes[0].Draw("same LPE")
gFracTrueMatch_vsRes[0].SetLineColor(kGreen+1)

gFracFakeMatch_vsRes[0].Draw("same LPE")
gFracFakeMatch_vsRes[0].SetLineColor(kRed+1)

gFracTrueExp_vsRes[0].Draw("same LPE")
gFracTrueExp_vsRes[0].SetLineColor(kBlack)


leg = TLegend(0.7,0.68,0.88,0.88,"","brNDC");
leg.AddEntry(gFracClustered_vsRes[0], "Total clustered", "lp");          
leg.AddEntry(gFracTrueMatch_vsRes[0], "Correct match", "lp");          
leg.AddEntry(gFracFakeMatch_vsRes[0], "Fake match", "lp");          
leg.AddEntry(gFracTrueExp_vsRes[0], "Total real", "lp");          
leg.Draw();
  
cFracClustered_vsRes.Update()



cFracClustered_vsSigma = TCanvas("cFracClustered_vsSigma", "cFracClustered_vsSigma", 500, 500)

gFracTrueMatch_vsSigma[0].Draw("ALPE")
gFracTrueMatch_vsSigma[0].SetLineColor(kGreen)

gFracTrueMatch_vsSigma[0].SetMinimum(0)
gFracTrueMatch_vsSigma[0].SetMaximum(1.2)

gFracFakeMatch_vsSigma[0].Draw("same LPE")
gFracFakeMatch_vsSigma[0].SetLineColor(kRed)


leg = TLegend(0.7,0.68,0.88,0.88,"","brNDC");
for iRes in range(len(res)):
    gFracFakeMatch_vsSigma[iRes].Draw("same LPE")
    gFracFakeMatch_vsSigma[iRes].SetLineColor(kRed+iRes)
    
    gFracTrueMatch_vsSigma[iRes].Draw("same LPE")
    gFracTrueMatch_vsSigma[iRes].SetLineColor(kGreen+iRes)


    leg.AddEntry(gFracTrueMatch_vsSigma[iRes], str("Correct: %s"% res_string[iRes]), "lp");          
    leg.AddEntry(gFracFakeMatch_vsSigma[iRes], str("Fake: %s" % res_string[iRes]), "lp");          
    
    
leg.Draw();
  
cFracClustered_vsSigma.Update()


cFracClustered_vsAcut = []
cFracClustered_vsBcut = []

selSample = 0
for iRes in range(len(res)):
    cFracClustered_vsAcut.append(TCanvas("cFracClustered_vsAcut_res%s"%res_string[iRes], "cFracClustered_vsAcut_res%s"%res_string[iRes], 500, 500))

    gFracTrueMatch_vsAcut[selSample][iRes].Draw("ALPE")
    gFracTrueMatch_vsAcut[selSample][iRes].SetLineColor(kGreen)
    
    gFracTrueMatch_vsAcut[selSample][iRes].SetMinimum(0)
    gFracTrueMatch_vsAcut[selSample][iRes].SetMaximum(1.2)

    gFracFakeMatch_vsAcut[selSample][iRes].Draw("same LPE")
    gFracFakeMatch_vsAcut[selSample][iRes].SetLineColor(kRed)

    gFracTrueExp_vsAcut[selSample][iRes].Draw("same LPE")
    gFracTrueExp_vsAcut[selSample][iRes].SetLineColor(kBlack)
    
    gFracClustered_vsAcut[selSample][iRes].Draw("same LPE")
    gFracClustered_vsAcut[selSample][iRes].SetLineColor(kBlue)
    
    gFracRatioMatch_vsAcut[selSample][iRes].Draw("same LPE")
    gFracRatioMatch_vsAcut[selSample][iRes].SetLineColor(kYellow+1)

    leg = TLegend(0.7,0.68,0.88,0.88,"","brNDC");


    leg.AddEntry(gFracTrueMatch_vsAcut[selSample][iRes], str("Correct: %s"% res_string[iRes]), "lp");          
    leg.AddEntry(gFracFakeMatch_vsAcut[selSample][iRes], str("Fake: %s" % res_string[iRes]), "lp");          
    leg.AddEntry(gFracClustered_vsAcut[selSample][iRes], str("Clustered: %s" % res_string[iRes]), "lp");      
    
    
    leg.Draw();
  
    cFracClustered_vsAcut[iRes].Update()
    
    
for iRes in range(len(res)):
    cFracClustered_vsBcut.append(TCanvas("cFracClustered_vsBcut_res%s"%res_string[iRes], "cFracClustered_vsBcut_res%s"%res_string[iRes], 500, 500))

    gFracTrueMatch_vsBcut[selSample][iRes].Draw("ALPE")
    gFracTrueMatch_vsBcut[selSample][iRes].SetLineColor(kGreen)
    
    gFracTrueMatch_vsBcut[selSample][iRes].SetMinimum(0)
    gFracTrueMatch_vsBcut[selSample][iRes].SetMaximum(1.2)

    gFracFakeMatch_vsBcut[selSample][iRes].Draw("same LPE")
    gFracFakeMatch_vsBcut[selSample][iRes].SetLineColor(kRed)

    gFracTrueExp_vsBcut[selSample][iRes].Draw("same LPE")
    gFracTrueExp_vsBcut[selSample][iRes].SetLineColor(kBlack)
    
    gFracClustered_vsBcut[selSample][iRes].Draw("same LPE")
    gFracClustered_vsBcut[selSample][iRes].SetLineColor(kBlue)
    
    gFracRatioMatch_vsBcut[selSample][iRes].Draw("same LPE")
    gFracRatioMatch_vsBcut[selSample][iRes].SetLineColor(kYellow+1)

    leg = TLegend(0.7,0.68,0.88,0.88,"","brNDC");


    leg.AddEntry(gFracTrueMatch_vsBcut[selSample][iRes], str("Correct: %s"% res_string[iRes]), "lp");          
    leg.AddEntry(gFracFakeMatch_vsBcut[selSample][iRes], str("Fake: %s" % res_string[iRes]), "lp");          
    leg.AddEntry(gFracClustered_vsBcut[selSample][iRes], str("Clustered: %s" % res_string[iRes]), "lp");      
    
    
    leg.Draw();
  
    cFracClustered_vsBcut[iRes].Update()



cAngleTest = TCanvas("cAngleTest", "cAngleTest", 500, 500)
if (hAngleTest[0].GetMaximum()>0): hAngleTest[0].Scale(1./hAngleTest[0].GetMaximum())
hAngleTest[0].Draw()

if (hAngleTestTrue[0].GetMaximum()>0): hAngleTestTrue[0].Scale(1./hAngleTestTrue[0].GetMaximum())
hAngleTestTrue[0].Draw("same")


for iRes in range(len(res)):
    hAngleTest[iRes].SetLineColor(iRes+1)
    hAngleTest[iRes].Draw("same")

gPad.SetLogy()  
cAngleTest.Update()


cBoostTest = TCanvas("cBoostTest", "cBoostTest", 500, 500)
if (hBoostTest[0].GetMaximum()>0): hBoostTest[0].Scale(1./hBoostTest[0].GetMaximum())
hBoostTest[0].Draw()

if (hBoostTestTrue[0].GetMaximum()>0): hBoostTestTrue[0].Scale(1./hBoostTestTrue[0].GetMaximum())
hBoostTestTrue[0].Draw("same")


for iRes in range(len(res)):
    hBoostTest[iRes].SetLineColor(iRes+1)
    hBoostTest[iRes].Draw("same")
  
gPad.SetLogx()  
cBoostTest.Update()


cBoost_vs_Angle = TCanvas("cBoost_vs_Angle", "cBoost_vs_Angle", 1000, 500)
cBoost_vs_Angle.Divide(2,1)
cBoost_vs_Angle.cd(1)
hBoost_vs_Angle_True[0].Draw("COLZ")

cBoost_vs_Angle.cd(2)
hBoost_vs_Angle[0].Draw("COLZ")

#if (hBoostTestTrue[0].GetMaximum()>0): hBoostTestTrue[0].Scale(1./hBoostTestTrue[0].GetMaximum())
#hBoostTestTrue[0].Draw("same")


#for iRes in range(len(res)):
    #hBoostTest[iRes].SetLineColor(iRes+1)
    #hBoostTest[iRes].Draw("same")
  
#gPad.SetLogx()  
cBoost_vs_Angle.Update()


#cMassTest = TCanvas("cMassTest", "cMassTest", 500, 500)
#if (hMassTest[0].GetMaximum()>0): hMassTest[0].Scale(1./hMassTest[0].GetEntries())
#hMassTest[0].Draw()

#if (hMassTestTrue[0].GetMaximum()>0): hMassTestTrue[0].Scale(1./hMassTestTrue[0].GetEntries())
#hMassTestTrue[0].Draw("same")
#hMassTestTrue[0].SetLineColor(kGreen+1)

#for iRes in range(len(res)):
    #hMassTest[iRes].SetLineColor(iRes+1)
    #hMassTest[iRes].Draw("same")
  
##gPad.SetLogx()  
#cMassTest.Update()


outputFile = TFile ("resultsBlossomFullStat.root", "RECREATE")
outputFile.cd()

for iSample in range(len(sample)):
    for iRes in range(len(res)):
        gFracTrueMatch_vsAcut[iSample][iRes].SetName("gFracTrueMatch_vsAcut_sample_%s_res%s" %(sample[iSample],res_string[iRes]))
        gFracFakeMatch_vsAcut[iSample][iRes].SetName("gFracFakeMatch_vsAcut_sample_%s_res%s" %(sample[iSample],res_string[iRes]))
        gFracTrueExp_vsAcut[iSample][iRes].SetName  ("gFracTrueExp_vsAcut_sample_%s_res%s"   %(sample[iSample],res_string[iRes]))
        gFracClustered_vsAcut[iSample][iRes].SetName("gFracClustered_vsAcut_sample_%s_res%s" %(sample[iSample],res_string[iRes]))
    
        gFracTrueMatch_vsAcut[iSample][iRes].Write()
        gFracFakeMatch_vsAcut[iSample][iRes].Write()
        gFracTrueExp_vsAcut[iSample][iRes].Write()
        gFracClustered_vsAcut[iSample][iRes].Write()
    
outputFile.Write()



input('Press ENTER to exit')
